package com.eab.hibernate.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="AUD_API_ACCESS")
public class AUD_API_ACCESS {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="AUD_ID", nullable=false, unique=true, length=20)
	private String AUD_ID;
	
	@Column(name="ACCESS_URL", length=255, nullable=true)
	private String ACCESS_URL;
	
	@Column(name="USER_ID", length=50, nullable=true)
	private String USER_ID;
	
	@Column(name="REMOTE_HOST", length=50, nullable=true)
	private String REMOTE_HOST;
	
	@Column(name="LOCAL_HOST", length=50, nullable=true)
	private String LOCAL_HOST;
	
	@Column(name="START_DT", nullable=false)
	private Timestamp START_DT;
	
	@Column(name="END_DT", nullable=true)
	private Timestamp END_DT;
	
	@Column(name="STATUS", length=1, nullable=false)
	private String STATUS;
	
	@Column(name="FAIL_REASON", length=500, nullable=true)
	private String FAIL_REASON;

	public String getAUD_ID() {
		return AUD_ID;
	}

	public void setAUD_ID(String aUD_ID) {
		AUD_ID = aUD_ID;
	}

	public String getACCESS_URL() {
		return ACCESS_URL;
	}

	public void setACCESS_URL(String aCCESS_URL) {
		ACCESS_URL = aCCESS_URL;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getREMOTE_HOST() {
		return REMOTE_HOST;
	}

	public void setREMOTE_HOST(String rEMOTE_HOST) {
		REMOTE_HOST = rEMOTE_HOST;
	}

	public String getLOCAL_HOST() {
		return LOCAL_HOST;
	}

	public void setLOCAL_HOST(String lOCAL_HOST) {
		LOCAL_HOST = lOCAL_HOST;
	}

	public Timestamp getSTART_DT() {
		return START_DT;
	}

	public void setSTART_DT(Timestamp sTART_DT) {
		START_DT = sTART_DT;
	}

	public Timestamp getEND_DT() {
		return END_DT;
	}

	public void setEND_DT(Timestamp eND_DT) {
		END_DT = eND_DT;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getFAIL_REASON() {
		return FAIL_REASON;
	}

	public void setFAIL_REASON(String fAIL_REASON) {
		FAIL_REASON = fAIL_REASON;
	}
	
}
