package com.eab.template;

final class Email {
	private String subject;
	private String template;
	
	public Email() {	
	}
	
	public Email(String subject, String template) {
		this.subject = subject;
		this.template = template;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
}

public final class NotificationsTemplate {
	private String[] keys;
	private String sms;
	private Email email;
	
	public NotificationsTemplate() {
	}
	
	public NotificationsTemplate(String[] keys, String sms, Email email) {
		this.keys = keys;
		this.sms = sms;
		this.email = email;
	}

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}
	
	public String getSubject() {
		return email.getSubject();
	}
	
	public String getTemplate() {
		return email.getTemplate();
	}
}
