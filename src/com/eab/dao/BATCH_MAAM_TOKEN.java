package com.eab.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class BATCH_MAAM_TOKEN {
	public boolean create(String jobID, String jobClass, String hostname, Date createDT, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "insert into BATCH_MAAM_TOKEN(job_id, job_class, local_host, start_dt, status) "
					+ "values("
					+ "" + dbm.param(jobID, DataType.TEXT)
					+ ", " + dbm.param(jobClass, DataType.TEXT)
					+ ", " + dbm.param(hostname, DataType.TEXT)
					+ ", " + dbm.param(createDT, DataType.DATE)
					+ ", 'P'"
					+ ")";
			result = dbm.insert(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
	
	public boolean update(String jobID, String hostname, Date createDT, Date updateDT, String status, String reason, Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		boolean result = false;
		
		try {
			String sql = "update BATCH_MAAM_TOKEN "
					+ "set end_dt=" + dbm.param(updateDT, DataType.DATE)
					+ ", status=" + dbm.param(status, DataType.TEXT)
					+ ", fail_reason=" + dbm.param(reason, DataType.TEXT)
					+ " where job_id=" + dbm.param(jobID, DataType.TEXT)
					+ " and local_host=" + dbm.param(hostname, DataType.TEXT)
					+ " and start_dt=" + dbm.param(createDT, DataType.DATE)
					;
			
			int count = dbm.update(sql, conn);
			if (count == 1)
				result = true;
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
}
