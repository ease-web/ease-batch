package com.eab.dao;

import com.eab.common.Log;
import com.eab.database.jdbc.DataType;

public class INVALIDATED_SIGNED_CASE extends BaseDao{
	public boolean create(int batchNo, String appNo, String paid)
			throws Exception {
		
		init();
		
		String sqlStatement = "insert into INVALIDATED_SIGNED_CASE (EAPP_NO, PAID, CREATE_DATE, CREATE_BY_BATCH_NO) "
				+  "values (" + dbm.param(appNo, DataType.TEXT) + ", "
				+  dbm.param(paid, DataType.TEXT) + ", sysdate,"
				+ dbm.param(batchNo, DataType.INT) + ")";
		
		Log.info(sqlStatement);
		return insert(sqlStatement);
	}
}
