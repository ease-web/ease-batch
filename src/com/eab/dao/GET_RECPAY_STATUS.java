package com.eab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;

public class GET_RECPAY_STATUS {
	public final static int IDLE_DAY = 7;
	
	public ResultSet enquiry(Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		ResultSet result = null;
		
		try {
			String sql = "select req_id, JSON_VALUE(param, '$.lob') as \"lob\""
					+ " from API_RLS_REC_PYMT_TRX a"
					+ " where status='C'"
					+ " and a.create_dt <= (sysdate - "+IDLE_DAY+")"
					+ " and not exists(select 1 from API_RLS_PYMT_NOTI_TRX b where b.req_id=a.req_id)"		//Callback
					+ " and not exists(select 1 from API_RLS_PYMT_STATUS_TRX c where c.req_id=a.req_id)"	//Get status
					+ " order by a.create_dt";
			result = dbm.select(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
}
