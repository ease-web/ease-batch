package com.eab.dao;

import com.eab.common.Log;
import com.eab.database.jdbc.DataType;

public class SUBMISSION_EXPIRED_CASE extends BaseDao{
	public boolean create(int batchNo, String policyNumber, String appNo)
			throws Exception {
		
		init();
		
		String sqlStatement = "insert into SUBMISSION_EXPIRED_CASE (POLICY_NUMBER, EAPP_NO, CREATE_DATE, CREATE_BY_BATCH_NO) "
				+  "values (" + dbm.param(policyNumber, DataType.TEXT) + ", "
				+  dbm.param(appNo, DataType.TEXT) + ", sysdate,"
				+ dbm.param(batchNo, DataType.INT) + ")";
		
		Log.info(sqlStatement);
		return insert(sqlStatement);
	}
}
