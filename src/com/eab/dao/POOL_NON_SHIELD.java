package com.eab.dao;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.eab.database.jdbc.DataType;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class POOL_NON_SHIELD extends BaseDao{
	
	public int selectRowCount() throws Exception {
		init();

		String sqlStatement = "select count(*) from POOL_NON_SHIELD where AUD_ID is null";

		return selectSingleRecord(sqlStatement).getInt("COUNT(*)");
	}
	
	public JSONArray selectUsedPolicyNumbers() throws Exception {
		init();

		String sqlStatement = "select POL_NUM, AUD_ID from POOL_NON_SHIELD where AUD_ID is not null";

		return selectMultipleRecords(sqlStatement);
	}
	
	public boolean insertRecordsFromArray(JsonArray jsonArray) throws Exception {
		init();
		
		String sqlStatement = "insert into POOL_NON_SHIELD(POL_NUM) values(?)";
		
		List<Integer> dataTypeList = new ArrayList<>(); // data type of that statement columns.
		dataTypeList.add(DataType.TEXT);
		dataTypeList.add(DataType.TEXT);
		
		List<Object> sqlStatementDataObjList = new ArrayList<>();
		
		for (JsonElement item : jsonArray) {
			String policyNumber = item.getAsString();
			
			List<Object> dataObjList = new ArrayList<>();
			dataObjList.add(policyNumber);
			
			sqlStatementDataObjList.add(dataObjList); //each statement will have a single object list.
			
		}

		return insertBatch(sqlStatement, sqlStatementDataObjList, dataTypeList);
	}

}
