package com.eab.dao;

import org.json.JSONObject;

public abstract class ReportTransactionDao extends BaseDao {
	public JSONObject selectLastCheckPointTime() throws Exception {
		return null;
	}
	
	public boolean create(String UUID) throws Exception {
		return false;
	}
	
	public boolean updateCountFromLocalDB(String UUID, java.sql.Date FROM_DT, java.sql.Date TO_DT, int CASE_NUM) throws Exception {
		return false;
	}
	
	public boolean updateCountFromCouchbase(String UUID, int CASE_NUM) throws Exception {
		return false;
	}
	
	public boolean updateAgentCountFromCouchbase(String UUID, int REQ_AGENT_NUM, int RESP_AGENT_NUM) throws Exception {
		return false;
	}
	
	public boolean updateFromSendingEmail(String UUID, String FILE_NAME, java.sql.Date EMAIL_DT, boolean isCompleted) throws Exception {
		return false;
	}
}
