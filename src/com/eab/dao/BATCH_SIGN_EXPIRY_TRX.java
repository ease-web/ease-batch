package com.eab.dao;

import java.util.Date;

import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.database.jdbc.DataType;

public class BATCH_SIGN_EXPIRY_TRX extends BaseDao{

	public boolean create(String appId, int batchNo, String polNo, String bundleId, Date signDate, int expDay, String premInd, String remark)
			throws Exception {
		
		init();
		
		Date currentDate = new Date();

		String sqlStatement = "insert into BATCH_SIGN_EXPIRY_TRX("+
				"EAPP_NO," + 
				"BATCH_NO," + 
				"POL_NO," + 
				"BUNDLE_ID," + 
				"SIGNATURE_DATE," + 
				"EXPIRY_DAY," + 
				"CREATE_DATE," + 
				"PREMIUM_IND, REMARK" 
				+  ") values (" 
				+ dbm.param(appId, DataType.TEXT) + ", "
				+ dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(polNo, DataType.TEXT) + ", "
				+ dbm.param(bundleId, DataType.TEXT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(signDate), DataType.DATE) + ", "
				+ dbm.param(expDay, DataType.INT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(premInd, DataType.TEXT) + ", "
				+ dbm.param(remark, DataType.TEXT) + ")";
		boolean result = insert(sqlStatement);
		return result;
	}

	public boolean exists(String appId) throws Exception {
		init();
		
		String sql = " select count(1) from BATCH_SIGN_EXPIRY_TRX where EAPP_NO = " + dbm.param(appId, DataType.TEXT);
		JSONObject result = selectSingleRecord(sql);
		return result != null;
	}

	public boolean update(String appId, int bNo, String remark) throws Exception {
		init();
		
		String sql = " update BATCH_SIGN_EXPIRY_TRX set REMARK = "+dbm.param(remark, DataType.TEXT)
		+" where EAPP_NO = " + dbm.param(appId, DataType.TEXT) + " AND BATCH_NO = " + dbm.param(bNo, DataType.INT);
		return update(sql);
	}


}
