package com.eab.dao;

import java.util.Date;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DataType;

public class BATCH_LOG extends BaseDao{

	public boolean create(int batchNo, String batchJobName, String batchStatus, Date curTime, int noOfIdentified)
			throws Exception {
		
		init();
		Log.info("Log Time");

		String sqlStatement = "insert into BATCH_LOG(BATCH_NO, BATCH_JOB_NAME, BATCH_STATUS, BATCH_DATE_ST, NO_REC_IDENTIFIED, RUN_BY, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY) "
				+  "values (" + dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(batchJobName, DataType.TEXT) + ", " 
				+ dbm.param(batchStatus, DataType.TEXT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE) + ", " 
				+ dbm.param(noOfIdentified, DataType.INT) + ", 'BATCH', " 
				+ dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE) + " , 'BATCH', " 
				+ dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE) + " , 'BATCH')";
		
		Log.info(sqlStatement);
		return insert(sqlStatement);
	}

	public boolean update(int batchNo, int recordProcessed, String batchStatus, String updatedBy) throws Exception {
		
		init();
		
		Date curTime = new Date();

		String sqlStatement = "UPDATE BATCH_LOG SET NO_REC_PROCESSED = " + dbm.param(recordProcessed, DataType.INT)
				+ " , BATCH_DATE_END = " + dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE)
				+ " , BATCH_STATUS = " + dbm.param(batchStatus, DataType.TEXT)
				+ " , UPD_DATE = " + dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE)
				+ " , UPD_BY = " + dbm.param(updatedBy, DataType.TEXT)
				+ " WHERE BATCH_NO = "
				+ dbm.param(batchNo, DataType.INT);

		return update(sqlStatement);
	}
	
	public int selectSeq() throws Exception{
		init();
		
		String sqlStatement = "select batch_log_no_seq.nextval from dual";
		
		return selectSingleRecord(sqlStatement).getInt("NEXTVAL");
	}
}
