package com.eab.dao;

import org.json.JSONObject;

import com.eab.database.jdbc.DataType;

public class RPT_DIRECT_SUBMIT_TRX extends ReportTransactionDao {
	
	@Override
	public JSONObject selectLastCheckPointTime() throws Exception {
		init();
		
		String sqlStatement = "SELECT to_char( cast(MAX(TO_DT) as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as MAX_DT FROM RPT_DIRECT_SUBMIT_TRX WHERE STATUS = 'C'";

		return selectSingleRecord(sqlStatement);

	}

	@Override
	public boolean create(String UUID) throws Exception {

		init();

		String sqlStatement = "insert into RPT_DIRECT_SUBMIT_TRX(UUID)" + " values(" + dbm.param(UUID, DataType.TEXT)
				+ ")";

		return insert(sqlStatement);

	}

	@Override
	public boolean updateCountFromLocalDB(String UUID, java.sql.Date FROM_DT, java.sql.Date TO_DT, int CASE_NUM) throws Exception {

		init();

		String sqlStatement = "UPDATE RPT_DIRECT_SUBMIT_TRX SET FROM_DT = " + dbm.param(FROM_DT, DataType.DATE)
				+ " , TO_DT = " + dbm.param(TO_DT, DataType.DATE) + " , CASE_NUM = " + dbm.param(CASE_NUM, DataType.INT)
				+ " WHERE UUID = " + dbm.param(UUID, DataType.TEXT);

		return update(sqlStatement);

	}

	@Override
	public boolean updateCountFromCouchbase(String UUID, int CASE_NUM) throws Exception {

		init();

		String sqlStatement = "UPDATE RPT_DIRECT_SUBMIT_TRX SET DIRECT_CASE_NUM = "
				+ dbm.param(CASE_NUM, DataType.INT) + " WHERE UUID = " + dbm.param(UUID, DataType.TEXT);

		return update(sqlStatement);

	}

	@Override
	public boolean updateAgentCountFromCouchbase(String UUID, int REQ_AGENT_NUM, int RESP_AGENT_NUM) throws Exception {

		init();

		String sqlStatement = "UPDATE RPT_DIRECT_SUBMIT_TRX SET REQ_AGENT_NUM = "
				+ dbm.param(REQ_AGENT_NUM, DataType.INT) + " , RESP_AGENT_NUM = " + dbm.param(RESP_AGENT_NUM, DataType.INT)
				+ " WHERE UUID = " + dbm.param(UUID, DataType.TEXT);

		return update(sqlStatement);

	}
	
	@Override
	public boolean updateFromSendingEmail(String UUID, String FILE_NAME, java.sql.Date EMAIL_DT, boolean isCompleted) throws Exception {

		init();
		
		String status = isCompleted ? "C" : "F";

		String sqlStatement = "UPDATE RPT_DIRECT_SUBMIT_TRX SET FILE_NAME = "
				+ dbm.param(FILE_NAME, DataType.TEXT) 
				+ " , EMAIL_DT = " + dbm.param(EMAIL_DT, DataType.DATE)
				+ " , STATUS = " + dbm.param(status, DataType.TEXT)
				+ " WHERE UUID = " + dbm.param(UUID, DataType.TEXT);

		return update(sqlStatement);

	}
}
