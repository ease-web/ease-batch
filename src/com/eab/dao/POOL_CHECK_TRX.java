package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class POOL_CHECK_TRX extends BaseDao {
	
	public boolean create(String uuid) throws Exception {

		init();

		String sqlStatement = "insert into POOL_CHECK_TRX(UUID)" + " values("
				+ dbm.param(uuid, DataType.TEXT)
				+ ")";

		return insert(sqlStatement);
	}
	
	public boolean update(String uuid, int countShieldPool, int countNonShieldPool, int POOL_SHIELD_SIZE, int POOL_NON_SHIELD_SIZE, int requestShieldNum, int requestNonShieldNum)
			throws Exception {

		init();
		
		String sqlStatement = "UPDATE POOL_CHECK_TRX SET SHLD_CURR_NUM = "
				+ dbm.param(countShieldPool, DataType.INT) 
				+ " , NON_SHLD_CURR_NUM = " + dbm.param(countNonShieldPool, DataType.INT) 
				+ " , SIZE_POOL_SHLD = " + dbm.param(POOL_SHIELD_SIZE, DataType.INT) 
				+ " , SIZE_POOL_NON_SHLD = " + dbm.param(POOL_NON_SHIELD_SIZE, DataType.INT) 
				+ " , REQ_SHLD_NUM = " + dbm.param(requestShieldNum, DataType.INT) 
				+ " , REQ_NON_SHLD_NUM = " + dbm.param(requestNonShieldNum, DataType.INT) 
				+ " WHERE UUID = "+ dbm.param(uuid, DataType.TEXT);

		return update(sqlStatement);
	}
	
	public boolean updateResponseShieldNumber(String uuid, int responseShieldNumber)
			throws Exception {

		init();
		
		String sqlStatement = "UPDATE POOL_CHECK_TRX SET RESP_SHLD_NUM = RESP_SHLD_NUM + "
				+ dbm.param(responseShieldNumber, DataType.INT) + " WHERE UUID = "+ dbm.param(uuid, DataType.TEXT);

		return update(sqlStatement);
	}

	public boolean updateResponseNonShieldNumber(String uuid, int responseNonShieldNumber)
			throws Exception {

		init();

		String sqlStatement = "UPDATE POOL_CHECK_TRX SET RESP_NON_SHLD_NUM = RESP_NON_SHLD_NUM + "
				+ dbm.param(responseNonShieldNumber, DataType.INT) + " WHERE UUID = "+ dbm.param(uuid, DataType.TEXT);

		return update(sqlStatement);
	}
}
