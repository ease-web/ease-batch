package com.eab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.ZonedDateTime;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;

public class BATCH_SUBMISSION_REQUEST extends BaseDao{

	public boolean create(String appNo, String statusToTrigger, int batchNo, String submissionStatus, String needToSubmitWFI, String needToSubmitRLS, String creartedBy)
			throws Exception {
		
		init();
		
		ZonedDateTime currentDate = Function.getDateUTCNow();
		
		String sqlStatement = "insert into BATCH_SUBMISSION_REQUEST(EAPP_NO, CREATE_DATE, TRIGGER_BY_PROCESS, CREATE_BY_BATCH_NO, SUBMISSION_DATE_ST, SUBMISSION_STATUS, WFI_SUBMISSION_STATUS, RLS_SUBMISSION_STATUS, CREATED_BY) "
				+  "values (" + dbm.param(appNo, DataType.TEXT) + ", "
				+ dbm.param(Function.covertZonedDateTimeToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(statusToTrigger, DataType.TEXT) + ", "
				+ dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(Function.covertZonedDateTimeToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(submissionStatus, DataType.TEXT) + ", "
				+ dbm.param(needToSubmitWFI, DataType.TEXT) + ", "
				+ dbm.param(needToSubmitRLS, DataType.TEXT) + ", "
				+ dbm.param(creartedBy, DataType.TEXT) + ")";

		return insert(sqlStatement);
	}
	
	public boolean hasPrimaryKey(String key, String status) throws Exception{
		Connection conn = null;		
		
		PreparedStatement stmt = null;
		
		String result = null;
		 
		try {
			conn = DBAccess.getConnection();	
			String sql = " select EAPP_NO"
					   + " from BATCH_SUBMISSION_REQUEST"
					   + " where EAPP_NO = ? and TRIGGER_BY_PROCESS = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, key);
			stmt.setString(2, status);
			
			ResultSet rs = stmt.executeQuery();

			if(rs != null && rs.next()){
				return true;
			} else {
				return false;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {				 
				Log.error(e2);
			}
		}
	}
	
	public int selectSeq() throws Exception{
		init();
		
		String sqlStatement = "select batch_sub_request_seq.nextval from dual";
		
		return selectSingleRecord(sqlStatement).getInt("NEXTVAL");
	}
	
	public JSONObject singlerecordquerySQL(String Sql) throws Exception {
		init();
		
		return selectSingleRecord(Sql); 
	}
	
	public JSONArray querySQL(String Sql) throws Exception {
		init();
		
		return selectMultipleRecords(Sql); 
	}
	
	public JSONArray selectSubmittedCasesToRLS(java.sql.Date fromTime, java.sql.Date toTime) throws Exception {
		init();
		
		String sqlStatement = "SELECT EAPP_NO, to_char( cast(RLS_ASYNC_CALLBACK_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as RLS_SUBMISSION_DATE, TRIGGER_BY_PROCESS FROM BATCH_SUBMISSION_REQUEST WHERE ((SUBMISSION_STATUS = 'C' AND WFI_SUBMISSION_STATUS is null AND RLS_SUBMISSION_STATUS is null) OR RLS_SUBMISSION_STATUS = 'C')  AND (RLS_ASYNC_CALLBACK_DATE BETWEEN "+ dbm.param(fromTime, DataType.DATE)+" AND "+dbm.param(toTime, DataType.DATE)+") ORDER BY RLS_ASYNC_CALLBACK_DATE ASC";
				
		System.out.println("Submitted Cases to RLS");
		System.out.println(sqlStatement);
		return selectMultipleRecords(sqlStatement); 
	}
	
	public JSONArray selectReSubmittedCasesToRLSOriginalStatus(java.sql.Date fromTime, java.sql.Date toTime) throws Exception {
		init();
		
		String sqlStatement = "SELECT bsra.EAPP_NO, bsra.TRIGGER_BY_PROCESS, bsra.CREATE_DATE"
				+ " FROM BATCH_SUBMISSION_REQUEST bsra"
				+ " INNER JOIN ("
				+ " SELECT EAPP_NO, MIN(CREATE_DATE) MIN_CREATE_DATE FROM BATCH_SUBMISSION_REQUEST WHERE EAPP_NO IN (SELECT DISTINCT EAPP_NO FROM BATCH_SUBMISSION_REQUEST WHERE SUBMISSION_STATUS = 'C' AND (RLS_ASYNC_CALLBACK_DATE BETWEEN "+ dbm.param(fromTime, DataType.DATE)+" AND "+dbm.param(toTime, DataType.DATE)+") AND TRIGGER_BY_PROCESS = 'API_RESUBMISSION') GROUP BY EAPP_NO"
				+ " ) bsrb ON bsra.EAPP_NO = bsrb.EAPP_NO AND bsra.CREATE_DATE = bsrb.MIN_CREATE_DATE";
		
		return selectMultipleRecords(sqlStatement); 
	}
	
	public boolean rollbackForResubmission(String appId) throws Exception {
		init();
		
		String sqlStatement = "UPDATE BATCH_SUBMISSION_REQUEST set Taken_By_Batch_No = null, SUBMISSION_STATUS = 'R' where EAPP_NO = " + dbm.param(appId, DataType.TEXT);

		return update(sqlStatement);
	}
}
