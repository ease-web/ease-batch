package com.eab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.eab.database.jdbc.DBManager;

public class GET_ESUB_STATUS {
	public final static int IDLE_DAY = 7;
	
	public ResultSet enquiry(Connection conn) throws SQLException, Exception {
		DBManager dbm = new DBManager();
		ResultSet result = null;
		
		try {
			String sql = "select a.pol_num"
					+ ", REPLACE(REGEXP_SUBSTR(dbms_lob.substr(a.resp_header, 4000, 1), 'x-axa-async-request-id(.*)'), 'x-axa-async-request-id: ', '') as \"request_id\""
					+ ", JSON_VALUE(param, '$.lob') as \"lob\""
					+ " from api_rls_rec_app_trx a"
					+ " where a.status='C'"
					+ " and a.create_dt <= (sysdate - "+IDLE_DAY+")"
					+ " and not exists (select 1 from api_rls_app_status b where a.pol_num = b.pol_num)"
					+ " order by a.create_dt";
			result = dbm.select(sql, conn);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		
		return result;
	}
}
