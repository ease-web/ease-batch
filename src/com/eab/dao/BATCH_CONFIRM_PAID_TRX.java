package com.eab.dao;

import java.util.Date;

import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.database.jdbc.DataType;

public class BATCH_CONFIRM_PAID_TRX extends BaseDao{

	public boolean create(String appId, int batchNo, String polNo, String bundleId, String premInd)
			throws Exception {
		
		init();
		
		Date currentDate = new Date();

		String sqlStatement = "insert into BATCH_CONFIRM_PAID_TRX("+
				"EAPP_NO," + 
				"BATCH_NO," + 
				"POL_NO," + 
				"BUNDLE_ID," +
				"CREATE_DATE," + 
				"PREMIUM_IND" 
				+  ") values (" 
				+ dbm.param(appId, DataType.TEXT) + ", "
				+ dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(polNo, DataType.TEXT) + ", "
				+ dbm.param(bundleId, DataType.TEXT) + ", "
				+ dbm.param(Function.convertJavaDateToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(premInd, DataType.TEXT) + ")";
		boolean result = insert(sqlStatement);
		return result;
	}

	public boolean exists(String appId) throws Exception {
		init();
		
		String sql = " select count(1) from BATCH_SIGN_EXPIRY_TRX where EAPP_NO = " + dbm.param(appId, DataType.TEXT);
		JSONObject result = selectSingleRecord(sql);
		return result != null;
	}


}
