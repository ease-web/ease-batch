package com.eab.dao;

import org.json.JSONArray;

public class API_PAYMENT_TRX extends BaseDao {

	public JSONArray selectInProgressRecords() throws Exception {
		init();

		//only select records within 2 hours
		String sqlStatement = "select TRX_NO, APP_ID, POLICY_NO, CURRENCY, AMOUNT, POL_TYPE, METHOD from API_PAYMENT_TRX where from_tz(\"TRX_TIMESTAMP\", 'UTC') + INTERVAL '2' HOUR >= current_timestamp at time zone 'UTC' and STATUS = 'I'";

		return selectMultipleRecords(sqlStatement);

	}

	public boolean updateStatusForTimeoutRecords() throws Exception {

		init();

		//records created over 2 hours are time-out
		String sqlStatement = "UPDATE API_PAYMENT_TRX SET STATUS = 'O' WHERE from_tz(\"TRX_TIMESTAMP\", 'UTC') + INTERVAL '2' HOUR < current_timestamp at time zone 'UTC' and STATUS = 'I'";

		return update(sqlStatement);
	}
	
	public JSONArray selectTimeoutRecords() throws Exception {

		init();
		//select status 'I' and over 2 hours 
		String sqlStatement = "SELECT TRX_NO, APP_ID, POLICY_NO, CURRENCY, AMOUNT, POL_TYPE, METHOD from API_PAYMENT_TRX WHERE from_tz(\"TRX_TIMESTAMP\", 'UTC') + INTERVAL '2' HOUR < current_timestamp at time zone 'UTC' and STATUS = 'I'";
		return selectMultipleRecords(sqlStatement);
	}

}
