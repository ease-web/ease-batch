package com.eab.utils;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.enumeration.NotificationsType;
import com.eab.exception.InvalidPropertiesException;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.template.NotificationsTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

public class NotificationsUtil {
	
	private static final String DEFAULT_SENDER = "ease@axa.com.sg";
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private static final String DIRECTOR_NOTIFICATION_PATH = Constant.TEMPLATE_PATH + "directorNotification.json";
	private static final String CASE_APPROVAL_REMINDER_PATH = Constant.TEMPLATE_PATH + "caseApprovalReminder.json";
	private static final String APPROVAL_EXPIRY_NOTIFICATION_PATH = Constant.TEMPLATE_PATH + "approvalExpiryNotification.json";
	private static final String SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION_PATH = Constant.TEMPLATE_PATH + "secondaryProxyAssignmentNotification.json";
	private static final String FAFIRM_NOTIFICATION_PATH = Constant.TEMPLATE_PATH + "faFirmReminder.json";
	private static final String FAFIRM_CASE_APPROVAL_REMINDER_PATH = Constant.TEMPLATE_PATH + "faFirmCaseApprovalReminder.json";
	private static final String HEALTH_DECLARATION_REMINDER_PATH = Constant.TEMPLATE_PATH + "approvalHealthDeclaration.json";
	
	
	private static String constructString(String[] needToReplaces, HashMap<String, String>props, String source) {		  
		for(String needToReplace : needToReplaces) {
			source = source.replace(needToReplace, props.get(needToReplace));
		}
		return source;
	}
	
	public static boolean isValidProps(NotificationsType type, HashMap<String, String> props) {
		NotificationsTemplate notificationsTemplate = NotificationsUtil.getNotificationsTemplate(type);
		for (String key : notificationsTemplate.getKeys()) {
			if (!key.equals("recipients") && !props.containsKey(key)) {
				return false;
			}
		}
		return true;
	}
	
	private static String findMissingParameter(NotificationsType type, HashMap<String, String> props) {
		NotificationsTemplate notificationsTemplate = NotificationsUtil.getNotificationsTemplate(type);
		for (String key : notificationsTemplate.getKeys()) {
			if (!key.equals("recipients") && !props.containsKey(key)) {
				return key;
			}
		}
		return "";
	}
	
	public static NotificationsTemplate getNotificationsTemplate(NotificationsType type) {
		String jsonTemplatePath;
		switch (type) {
		case DIRECTOR_NOTIFICATION:
			jsonTemplatePath = DIRECTOR_NOTIFICATION_PATH;
			break;
		case CASE_APPROVAL_REMINDER:
			jsonTemplatePath = CASE_APPROVAL_REMINDER_PATH;
			break;
		case APPROVAL_EXPIRY_NOTIFICATION:
			jsonTemplatePath = APPROVAL_EXPIRY_NOTIFICATION_PATH;
			break;
		case SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
			jsonTemplatePath = SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION_PATH;
			break;
		case FA_FIRM_NOTIFICATION:
			jsonTemplatePath = FAFIRM_NOTIFICATION_PATH;
			break;
		case FA_FIRM_CASE_APPROVAL_REMINDER:
			jsonTemplatePath = FAFIRM_CASE_APPROVAL_REMINDER_PATH;
			break;
		case HEALTH_DECLARATION_REMINDER:
			jsonTemplatePath = HEALTH_DECLARATION_REMINDER_PATH;
			break;
		default:
			jsonTemplatePath = null;
		}
		if (jsonTemplatePath.equals("")) {
			return null;
		}
		try {
			try (FileReader fileReader = new FileReader(jsonTemplatePath); JsonReader jsonReader = new JsonReader(fileReader)) {
				Gson gson = new GsonBuilder().create();			
				return (NotificationsTemplate)gson.fromJson(jsonReader, NotificationsTemplate.class);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	
	private static String getTitle(NotificationsType type, HashMap<String, String> props) {
		switch (type) {
		case DIRECTOR_NOTIFICATION:
			NotificationsTemplate directorNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.DIRECTOR_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"proposalNumber", "productName", "agentName"}, 
					props, directorNotification.getSubject()
				  );
		case FA_FIRM_NOTIFICATION:
			NotificationsTemplate fafirmNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.FA_FIRM_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"proposalNumber", "productName", "agentName"}, 
					props, fafirmNotification.getSubject()
				  );
		case CASE_APPROVAL_REMINDER:
			NotificationsTemplate caseApprovalReminder = NotificationsUtil.getNotificationsTemplate(NotificationsType.CASE_APPROVAL_REMINDER);
			return caseApprovalReminder.getSubject();
		case FA_FIRM_CASE_APPROVAL_REMINDER:
			NotificationsTemplate faFirmCaseApprovalReminder = NotificationsUtil.getNotificationsTemplate(NotificationsType.FA_FIRM_CASE_APPROVAL_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"proposalNumber", "productName", "agentName"},  
					props, 
					faFirmCaseApprovalReminder.getSubject()
				   );
		case APPROVAL_EXPIRY_NOTIFICATION:
			NotificationsTemplate approvalExpiry = NotificationsUtil.getNotificationsTemplate(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"caseNumber", "productName"}, 
					props, 
					approvalExpiry.getSubject()
				   );
		case SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
			NotificationsTemplate secondaryProxyAssingmentNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"proposalNumber", "productName", "agentName"}, 
					props, 
					secondaryProxyAssingmentNotification.getSubject()
				   );
		case HEALTH_DECLARATION_REMINDER:
			NotificationsTemplate healthDeclarationNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.HEALTH_DECLARATION_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "proposalNumber", "proposerName"}, 
					props, 
					healthDeclarationNotification.getSubject()
				   );
		default:
			return "";
		}
	}
	
	private static String getContent(NotificationsType type, HashMap<String, String> props) {
		switch (type) {
		case DIRECTOR_NOTIFICATION:
			NotificationsTemplate directorNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.DIRECTOR_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "managerName", "submissionDate", "proposalNumber", "clientName", "expiryDate"}, 
					props, directorNotification.getTemplate()
				  );
		case FA_FIRM_NOTIFICATION:
			NotificationsTemplate fafirmNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.FA_FIRM_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "managerName", "submissionDate", "proposalNumber", "clientName", "expiryDate"}, 
					props, fafirmNotification.getTemplate()
				  );
		case CASE_APPROVAL_REMINDER:
			NotificationsTemplate caseApprovalReminder = NotificationsUtil.getNotificationsTemplate(NotificationsType.CASE_APPROVAL_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"actionLink"}, 
					props, caseApprovalReminder.getTemplate()
				  );
		case FA_FIRM_CASE_APPROVAL_REMINDER:
			NotificationsTemplate faFirmCaseApprovalReminder = NotificationsUtil.getNotificationsTemplate(NotificationsType.FA_FIRM_CASE_APPROVAL_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"actionLink","adviserName","submissionDate","proposalNumber","proposerName","expiryDate"}, 
					props, faFirmCaseApprovalReminder.getTemplate()
				  );
		case APPROVAL_EXPIRY_NOTIFICATION:
			NotificationsTemplate approvalExpiry = NotificationsUtil.getNotificationsTemplate(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"agentName", "caseNumber", "proposerName", "submissionDate"}, 
					props, approvalExpiry.getTemplate()
				  );
		case SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
			NotificationsTemplate secondaryProxyAssingmentNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "submissionDate", "proposalNumber", "proposerName", "expiryDate", "actionLink"}, 
					props, secondaryProxyAssingmentNotification.getTemplate()
				  );
		case HEALTH_DECLARATION_REMINDER:
			NotificationsTemplate healthDeclarationNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.HEALTH_DECLARATION_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "proposalNumber", "proposerName"}, 
					props, healthDeclarationNotification.getTemplate()
				  );
		default:
			return "";
		}
	}
	
	private static String getSMSMsg(NotificationsType type, HashMap<String, String> props) {
		switch (type) {
		case DIRECTOR_NOTIFICATION:
			NotificationsTemplate directorNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.DIRECTOR_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "managerName", "submissionDate", "proposalNumber", "clientName", "expiryDate"},
					props, directorNotification.getSms()
				  );
		case CASE_APPROVAL_REMINDER:
			NotificationsTemplate caseApprovalReminder = NotificationsUtil.getNotificationsTemplate(NotificationsType.CASE_APPROVAL_REMINDER);
			return caseApprovalReminder.getSms();
		case APPROVAL_EXPIRY_NOTIFICATION:
			NotificationsTemplate approvalExpiry = NotificationsUtil.getNotificationsTemplate(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "submissionDate", "proposalNumber", "proposerName"},
					props, approvalExpiry.getSms()
				  );
		case SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION:
			NotificationsTemplate secondaryProxyAssingmentNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION);
			return NotificationsUtil.constructString(
					new String[] {"productName", "adviserName", "submissionDate", "proposalNumber", "proposerName", "expiryDate"},
					props, secondaryProxyAssingmentNotification.getSms()
				  );
		case HEALTH_DECLARATION_REMINDER:
			NotificationsTemplate healthDeclarationNotification = NotificationsUtil.getNotificationsTemplate(NotificationsType.HEALTH_DECLARATION_REMINDER);
			return NotificationsUtil.constructString(
					new String[] {"adviserName", "proposalNumber", "proposerName"},
					props, healthDeclarationNotification.getSms()
				  );
		default:
			return "";
		}
	}

	public static Email getEmailObject(NotificationsType type, HashMap<String, String> props) throws InvalidPropertiesException {
		if (!NotificationsUtil.isValidProps(type, props)) {
			throw new InvalidPropertiesException(NotificationsUtil.findMissingParameter(type, props));
		}
		return new Email(
			DEFAULT_SENDER,
			props.get("recipients"),
			props.get("cc"),
			NotificationsUtil.getTitle(type, props),
			NotificationsUtil.getContent(type, props)
		);
	}
	
	public static SMS getSMSObject(NotificationsType type, HashMap<String, String> props) throws InvalidPropertiesException {
//		if (!NotificationsUtil.isValidProps(type, props)) {
//			throw new InvalidPropertiesException(NotificationsUtil.findMissingParameter(type, props));
//		}
		return new SMS(
			props.get("mobileNo"),
			NotificationsUtil.getSMSMsg(type, props)
		);
	}

	public static String sendEmail(Email email) throws Exception {
		JsonObject jsonBody = new JsonObject(); 		
		jsonBody.addProperty("from", email.getSender());
		jsonBody.addProperty("to", email.getRecipients());
		if (email.getCc() != null) {
			jsonBody.addProperty("cc", email.getCc());
		}
		jsonBody.addProperty("title", email.getTitle());
		jsonBody.addProperty("content", email.getContent()); 	
		jsonBody.add("attachments", getaxaLogo());
		
		return callApiSendEmail(jsonBody);
	}
	
	public static JsonArray getaxaLogo() throws Exception {
		JsonArray result = new JsonArray();
		try {
			try (FileReader fileReader = new FileReader(Constant.TEMPLATE_PATH + "axa_logo.json"); 
					JsonReader jsonReader = new JsonReader(fileReader)) {
				Gson gson = new GsonBuilder().create();			
				JsonElement elm =  gson.fromJson(jsonReader, JsonElement.class);
				result =  elm.getAsJsonObject().getAsJsonArray("axa_logo_attachment");
				return result;
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return result;
		}
	}
	
	public static String sendSms(SMS sms) throws Exception {
		JsonObject jsonBody = new JsonObject(); 		
		jsonBody.addProperty("mobileNo", sms.getMobileNo());
		jsonBody.addProperty("smsMsg", sms.getSmsMsg());
		
		Log.info("Print SMS");
		return callApiSendSms(jsonBody);
	}
	
	private static String callApiSendEmail(JsonObject body) throws Exception {
		HttpClient httpclient = HttpClients.createDefault();
		
		HttpPost httppost = new HttpPost(DOMAIN + "/email/sendEmail");
		httppost.setHeader("Content-Type", "application/json");
		
		httppost.setEntity(new StringEntity(body.toString(), "UTF-8"));

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);

		HttpEntity entity = response.getEntity();

		if (entity != null) {
			String responseString = EntityUtils.toString(response.getEntity());
			return  responseString;
		} else {
			return "";
		}
	}
	
	private static String callApiSendSms(JsonObject body) throws Exception {
		HttpClient httpclient = HttpClients.createDefault();
		
		HttpPost httppost = new HttpPost(DOMAIN + "/sms/sendSMS");
		httppost.setHeader("Content-Type", "application/json");
		
		httppost.setEntity(new StringEntity(body.toString(), "UTF-8"));

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);

		HttpEntity entity = response.getEntity();

		if (entity != null) {
			String responseString = EntityUtils.toString(response.getEntity());
			return  responseString;
		} else {
			return "";
		}
	}
	
	public static String convertISOStringtoDisplay(String ISOString, String displayFormatStr) throws Exception{
		String df;
		if (ISOString.length() > 10) {
			df = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
		}else {
			df = "yyyy-MM-dd";
		}
		Date dateValue = null;
		SimpleDateFormat sdf = new SimpleDateFormat(df);
		SimpleDateFormat displayFormat = new SimpleDateFormat(displayFormatStr);
		dateValue = sdf.parse(ISOString);
		Calendar c = Calendar.getInstance();
		c.setTime(dateValue);
		return displayFormat.format(c.getTime());
	}
}




