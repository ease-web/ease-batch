package com.eab.auth.ease;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import com.eab.common.EnvVariable;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Maam {
	//private static String MAAM_USERNAME = "866279d0-cd4e-4211-a4b7-85c3a707346a";
    //private static String MAAM_SECRET = "3651852b-3506-4c78-bcf5-e19784c516ee";
    
	public static boolean requestToken() throws Exception {
		boolean status = false;
		
		Log.info("Request Access Token - Start");
		String maamUsername = EnvVariable.get("MAAM_USERNAME");
		String maamSecret = EnvVariable.get("MAAM_SECRET");
        String maamHost = EnvVariable.get("WEB_API_MAAM");
        
        String auth = new String(Base64.getEncoder().encode((maamUsername + ":" + maamSecret).getBytes()));
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("grant_type", "client_credentials"));
        nameValuePairs.add(new BasicNameValuePair("scope", "axa-sg-lifeapi"));
        HttpPost request = new HttpPost(maamHost);
        request.addHeader("Accept", "*/*");
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        request.addHeader("Authorization", "Basic " + auth);
        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        ///TODO: create audit record for every MAAM request and token
        
        Response resp = HttpUtil.send(request);
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject json = gson.fromJson(resp.getEntity().toString(), JsonElement.class).getAsJsonObject();
        
        if (json != null && json.get("access_token") != null) {
        	Log.debug(json.toString());
        	MaamBean.accessToken = (json.get("access_token")!=null)?json.get("access_token").getAsString():null;
        	MaamBean.tokenType = (json.get("token_type")!=null)?json.get("token_type").getAsString():null;
        	MaamBean.expiresIn = (json.get("expires_in")!=null)?json.get("expires_in").getAsInt():0;
        	MaamBean.refreshToken = (json.get("refresh_token")!=null)?json.get("refresh_token").getAsString():null;
        	MaamBean.scope = (json.get("scope")!=null)?json.get("scope").getAsString():null;
        	MaamBean.updDateTime = new java.util.Date();
        	status = true;
        } else if (json != null && json.get("error") != null) {
        	Log.error(json.toString());
        }
        
        ///TODO: update audit record
        
        return status;
    }
	
	public static boolean refreshToken() throws Exception {
		boolean status = false;
		
		Log.info("Refresh Access Token - Start");
		String maamUsername = EnvVariable.get("MAAM_USERNAME");
		String maamSecret = EnvVariable.get("MAAM_SECRET");
        String maamHost = EnvVariable.get("WEB_API_MAAM");
        
        String auth = new String(Base64.getEncoder().encode((maamUsername + ":" + maamSecret).getBytes()));
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("grant_type", "refresh_token"));
        nameValuePairs.add(new BasicNameValuePair("refresh_token", MaamBean.refreshToken));
        HttpPost request = new HttpPost(maamHost);
        request.addHeader("Accept", "*/*");
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        request.addHeader("Authorization", "Basic " + auth);
        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        ///TODO: create audit record for every MAAM request and token
        
        Response resp = HttpUtil.send(request);
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject json = gson.fromJson(resp.getEntity().toString(), JsonElement.class).getAsJsonObject();
        
        if (json != null && json.get("access_token") != null) {
        	MaamBean.accessToken = (json.get("access_token")!=null)?json.get("access_token").getAsString():null;
        	MaamBean.tokenType = (json.get("token_type")!=null)?json.get("token_type").getAsString():null;
        	MaamBean.expiresIn = (json.get("expires_in")!=null)?json.get("expires_in").getAsInt():0;
        	MaamBean.refreshToken = (json.get("refresh_token")!=null)?json.get("refresh_token").getAsString():null;
        	MaamBean.scope = (json.get("scope")!=null)?json.get("scope").getAsString():null;
        	MaamBean.updDateTime = new java.util.Date();
        	status = true;
        	Log.debug(json.toString());
        } else if (json != null && json.get("error") != null) {
        	Log.error(json.toString());
        }
        
        ///TODO: update audit record
        
        return status;
    }
	
	public static String getToken() {
		return MaamBean.accessToken;
	}
}
