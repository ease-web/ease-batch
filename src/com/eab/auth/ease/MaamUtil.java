package com.eab.auth.ease;

import java.sql.Connection;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.BATCH_MAAM_TOKEN;
import com.eab.database.jdbc.DBAccess;

public class MaamUtil {
	private static final String JOB_ID = "MAAM_TOKEN_REQ";
	
	public static boolean updateToken(String className) {
		boolean needGet = false;
		boolean needRefresh = false;
		BATCH_MAAM_TOKEN dao = new BATCH_MAAM_TOKEN();
		Connection conn = null;
		Date createDT = new Date();
		boolean noError = true;
		String errReason = null;
		Log.info("BATCH: Run [" + className + "] at " + new java.util.Date());

		try {
			conn = DBAccess.getConnection();
			
			// Create Batch Audit
			dao.create(JOB_ID, className, Constant.APP_IP, Function.convertJavaDateToSqlDate(createDT), conn);
			
			// Check Access Token is Empty?
			if (MaamBean.accessToken == null || MaamBean.accessToken.length() == 0) {
				needGet = true;
			} else {
				// Check Access Token is expired?
				int checkSize = MaamBean.expiresIn - MaamBean.refreshBefore;
				if (checkSize > 0) {
					Date currDate = new Date();
					Date alertDate = DateUtils.addSeconds(MaamBean.updDateTime, checkSize);
					if ( !currDate.before(alertDate) ) {
						needRefresh = true;
					}
				} else {
					needRefresh = true;
				}
			}
			
			if (needGet) {
				Log.info("BATCH: need get new token");
				Log.info("BATCH: result = " + Maam.requestToken());
			} else if (needRefresh) {
				Log.info("BATCH: need refresh exist token");
				if (MaamBean.refreshToken != null && MaamBean.refreshToken.length() > 0)
					Log.info("BATCH: result = " + Maam.refreshToken());
				else
					Log.info("BATCH: result = " + Maam.requestToken());
			}
		
		} catch (JobExecutionException e) {
			Log.error(e);
			noError = false;
			errReason = Function.extractException(e);
		} catch (Exception e) {
			Log.error(e);
			noError = false;
			errReason = Function.extractException(e);
		} finally {
			try {
				// Update Transaction Log in Database
				dao.update(JOB_ID, Constant.APP_IP, Function.convertJavaDateToSqlDate(createDT), Function.convertJavaDateToSqlDate(new Date()), ((noError)?"C":"F"), errReason, conn);
			} catch(Exception ex) {
				Log.error(ex);
			}
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
			
			dao = null;
		}
		
		return noError;
	}
}
