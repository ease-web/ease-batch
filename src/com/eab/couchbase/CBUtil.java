package com.eab.couchbase;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eab.common.Log;

public class CBUtil {
	public final String SAVE_FAILURE = "Cannot save document";
	public final String UPDATE_SUCCESS = "Update Success";
	public final String DELETE_FAILURE = "Delete Failure";
	public final String DELETE_SUCCESS = "Delete Success";
	public final String CONTENT_TYPE_JSON = "application/json";
	public final String CONTENT_TYPE_PDF = "application/pdf";
	public final String CONTENT_TYPE_PNG = "img/png";
	public final String CONTENT_TYPE_JPG = "img/jpeg";
	private static String gatewayURL;
	private static String gatewayPort;
	private static String gatewayDBName;
	private static String gatewayUser;
	private static String gatewayPW;


	public CBUtil(String gatewayURL, String gatewayPort, String gatewayDBName){
		this.gatewayURL = gatewayURL;
		this.gatewayPort = gatewayPort;
		this.gatewayDBName = gatewayDBName;
		this.gatewayUser = "";
		this.gatewayPW = "";
	}
	
	public CBUtil(String gatewayURL, String gatewayPort, String gatewayDBName, String gatewayUser, String gatewayPW){
		this.gatewayURL = gatewayURL;
		this.gatewayPort = gatewayPort;
		this.gatewayDBName = gatewayDBName;		
		this.gatewayUser = gatewayUser;
		this.gatewayPW = gatewayPW;
	}
	
	public String CreateDoc(String pk, String jsonFile) {
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(pk, gatewayUser, gatewayPW);
			
			if (con != null) {
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				
				String revId = getRevId(pk);;
				
				if (!revId.isEmpty())
					con.setRequestProperty("If-Match", revId);

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				InputStream inputStream = new ByteArrayInputStream(jsonFile.getBytes(Charset.forName("UTF-8")));
				BufferedInputStream bis = new BufferedInputStream(inputStream);
				byte[] buffer = jsonFile.getBytes(Charset.forName("UTF-8"));
				int b;
				
				while ((b = bis.read(buffer)) > 0) {
					bos.write(buffer, 0, b);
				}
				
				bos.close();
				bis.close();
				
				


				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();
				
				Log.info("{CBServer CreateDoc} ------------ Document:" + pk + "\t" + con.getResponseCode() + " - " + con.getResponseMessage());
			}
		} catch (IOException ex) {
			Log.error("{CBServer CreateDoc} --------> Exception");
			Log.error(ex);
			return null;
		} finally {
			if(con != null)
				con.disconnect();
		}
	
		return pk;
	}

	public String updateRole(String roleName, String roleJSON){
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection("_role/" + roleName ,gatewayUser, gatewayPW);
			if(con != null){	
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				InputStream inputStream = new ByteArrayInputStream(roleJSON.getBytes(Charset.forName("UTF-8")));
				BufferedInputStream bis = new BufferedInputStream(inputStream);
				byte[] buffer = roleJSON.getBytes(Charset.forName("UTF-8"));
				int b;
				
				while ((b = bis.read(buffer)) >0) {
					bos.write(buffer, 0, b);
				}
				bos.close();

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer UpdateRole} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}

	public String getListByKey(String startkey, String endkey){
		HttpURLConnection con = null;
		
		try {
			String tag = "_all_docs?startkey=" + startkey + "&endkey=" + endkey;
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}			
	} 

	public String UpdateDoc(String pk, String value) {
		if(CreateDoc(pk, value) == null)
			return SAVE_FAILURE;
		
		return  UPDATE_SUCCESS;
	}

	public boolean UpdateDocByDict(String pk, Map<String, Object> value){
		return true;
	}

	public String RemoveDoc(String pk) {
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(pk,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("DELETE");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer RemoveDoc} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}		
	}

	public void CreateAttach(String pk, String imageName, String fileName, InputStream fileIn) {
		HttpURLConnection con = null;
		
		try {
			String content_type = "";

			if(fileName.equals("application/pdf")){
				content_type = "application/pdf";
			}else if(fileName.equals("image/png")){
				content_type = "image/png";
			}else if(fileName.equals("image/jpeg")){
				content_type = "image/jpeg";
			}else{      		
				if (fileName.lastIndexOf(".") > 0) {    			  		
					String format = fileName.substring(fileName.lastIndexOf("."), fileName.length());
					if (format.equals(".png")) {
						content_type = "image/png";
					} else if (format.equals(".jpg")) {
						content_type = "image/jpeg";
					} else if (format.equals(".pdf")) {
						content_type = "application/pdf";
					} else if (format.equals(".json")) {
						content_type = "application/json";
					}
				}
			}
		
			String tag = pk + "/" + imageName;
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("PUT");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", content_type);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedOutputStream bos = new BufferedOutputStream(con.getOutputStream());
				BufferedInputStream bis = new BufferedInputStream(fileIn);
			
				int len = 0;
				byte[] buf = new byte[1024];
			
				while ((len = fileIn.read(buf, 0, 1024)) != -1) {
					bos.write(buf, 0, len);
				}
				bos.close();
			    		
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();
				bis.close();
			}
		} catch (IOException ex) {
			Log.error("{CBServer CreateAttach} --------> " + ex);
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}

	public String RemoveAttach(String pk, String iamgeName) {
		HttpURLConnection con=null;
		
		try {
			con = (HttpURLConnection)getURLConnection(pk + "/" + iamgeName,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("DELETE");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setRequestProperty("If-Match", getRevId(pk));

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return response.toString();
			}
			
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer RemoveDoc} --------> " + ex);
			return null;
		} finally{
			if(con!=null)
				con.disconnect();
		}	
	}     

	private static String getRevId(String docId){
		String rev = "";
		HttpURLConnection con = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(docId, gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");           

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// JSON
//				JSONObject jo_doc = new JSONObject(response.toString());
//				rev = jo_doc.getString("_rev");
				JSONObject jo_doc = null;
				try {
					jo_doc = new JSONObject(response.toString());
					rev = jo_doc.getString("_rev");
				} catch (JSONException ex) {
					String target = "\"_rev\":\"";
					String dummy = response.toString();
					int startpos = dummy.indexOf(target) + target.length();
					dummy = dummy.substring(startpos, startpos + 100);
					int endpos = dummy.indexOf("\"");
					dummy = dummy.substring(0, endpos);
					Log.debug("[kaming] customized _rev: " + dummy);
					
					rev = dummy;
				}
			}

		} catch (IOException ex) {
			Log.info("{CBServer getRevId} -------- Revision Not Find Create new Document ");
			return "";
		} catch (JSONException ex) {
			Log.error("{CBServer getRevId} --------> JSONException " + ex);
			Log.error(ex);
			return "";
		} finally {
			if(con!=null)
				con.disconnect();
		}	
		
		return rev;
	}

	private String getMIMEtype(String fileType)
	{
		String mimeType = "";

		if (fileType != null) {            
			if (fileType.equals(".png"))
				mimeType = "image/png";
			else if (fileType.equals(".jpg"))
				mimeType = "image/jpeg";
			else if (fileType.equals(".pdf")) 
				mimeType = "application/pdf";
			else if (fileType.equals(".json")) 
				mimeType = "application/json";
		}

		return mimeType;
	}

	public boolean checkDBConnection() {
		HttpURLConnection con=null;
		
		try {
			con = (HttpURLConnection)getURLConnection("" ,gatewayUser, gatewayPW);

			if(con != null)
				return true;
			else
				return false;
		} catch (IOException ex) {
			return false;
		} finally{
			if(con != null)
				con.disconnect();
		}	
	}
	
	private static URLConnection getURLConnection(String tag, String user, String password) throws IOException  {		 
		URLConnection urlConnection=null;
		
		try {		
			String authString = user + ":" + password;

			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			String urlStr = gatewayURL;
			if (StringUtils.isNotEmpty(gatewayPort)) {
				urlStr += ":" + gatewayPort + "/" + gatewayDBName + "/" + tag;
			} else {
				urlStr += "/" + gatewayDBName + "/" + tag;
			}
			Log.info("Connect to syn gateway:" + urlStr);
			URL url = new URL(urlStr);
			urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
		} catch (MalformedURLException e) {
			e.printStackTrace();				
		} catch (IOException e) {
			e.printStackTrace();				
		}  
		
		return urlConnection;
	}

	public JSONObject getDoc(String docId){
		HttpURLConnection con = null;
		JSONObject doc = null;
		
		try {
			con = (HttpURLConnection)getURLConnection(docId,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");           

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// JSON
				doc = new JSONObject(response.toString());
			}
		} catch (IOException ex) {
			Log.info("{CBServer getDoc} -------- Document Not Found " + ex);
			return null;
		} catch (JSONException ex) {
			Log.error("{CBServer getDoc} --------> JSONException " + ex);
			Log.error(ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
		
		return doc;
	}
	
	public byte[] GetAttach(String pk, String attachName, String fileType) {
		HttpURLConnection con =null;
		
		try {
			String url;             
			url = gatewayURL + ":" + gatewayPort + "/" + gatewayDBName + "/" + pk + "/" + attachName;
			//String destinationFile = "d:/" + attachName + fileType;

			URL obj;

			obj = new URL(url);
			Log.info("Get attachment from the link: " + pk + "/" + attachName);
			
			con = (HttpURLConnection)getURLConnection(pk + "/" + attachName,gatewayUser, gatewayPW);
			
			if(con!=null){
				con.setRequestMethod("GET");             

				InputStream  is = con.getInputStream();    
				//OutputStream os = new FileOutputStream(destinationFile);
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();

				byte[] b = new byte[16384];
				int length;
				
				while ((length = is.read(b, 0, b.length)) != -1) {
					buffer.write(b, 0, length);
				}

				//buffer.writeTo(os);
				is.close();     		
				//os.close(); 

				return buffer.toByteArray();  
			}
			return null;

		} catch (IOException ex) {
			Log.error("{CBServer GetAttach} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}	
	}

		
	public JSONObject getRecordsByView(String view, String startkey, String endkey){
		HttpURLConnection con = null;
		
		try {
			String tag = "_design/main/_view/" + view + "?startkey=" + startkey + "&endkey=" + endkey + "&stale=ok";
			Log.info("query view:" + tag);
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return new JSONObject(response.toString());
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}			
	}
	
	private JSONObject returnViewResults(String tag) {
		HttpURLConnection con = null;
		
		try {
			Log.info("query view after index:" + tag);
			
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return new JSONObject(response.toString());
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}
	}
	
	public JSONObject getRecordsByViewAfterIndex(String view, String startkey, String endkey, String mainViewPath){		
		try {
			String tag = "_design/" + mainViewPath + "/_view/" + view + "?startkey=" + startkey + "&endkey=" + endkey + "&stale=false";
			Log.info("Mobile query view after index:" + tag);
			return returnViewResults(tag);
		} catch (Exception ex) {
			Log.error("{Mobile CBServer getListByKey} --------> " + ex);
			return null;
		}		
	}
	

	public JSONObject getRecordsByViewAfterIndex(String view, String startkey, String endkey){		
		try {
			String tag = "_design/main/_view/" + view + "?startkey=" + startkey + "&endkey=" + endkey + "&stale=false";
			Log.info("query view after index:" + tag);
			return returnViewResults(tag);
		} catch (Exception ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}		
	}
	
	public JSONObject getRecordsByViewParamAfterIndex(String view, String params){
		HttpURLConnection con = null;
		
		try {
			String tag = "_design/main/_view/" + view + "?keys=" + params + "&stale=false";
			
			con = (HttpURLConnection)getURLConnection(tag,gatewayUser, gatewayPW);
			
			if(con != null){
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
				con.setConnectTimeout(10000);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();

				return new JSONObject(response.toString());
			}
			return null;
		} catch (IOException ex) {
			Log.error("{CBServer getListByKey} --------> " + ex);
			return null;
		}finally{
			if(con!=null)
				con.disconnect();
		}			
	}
	
	public JSONArray getDocsByView(String viewName, String[] ...keys) 
	{
		//----------------- Usage ---------------- //
		//String[] key1 = {"01", "102-3750787"};
		//String[] key2 = {"01", "102-3042664"};
		//String[] key3 = {"01", "102-3692559"};
		//JSONArray resultArray = getDocsByBatch("approvalApp",key1,key2,key3);
		//----------------- Usage ---------------- //
		
		
		int queryMaxLength = 1600; //URL max Chars 2048. reserved for base URL and buffer
		
		 ArrayList<String> queryStringArray = new ArrayList<String>();
		
		//Handling keys String query
		 int keyStringLengthCount = 0;
		 
		JSONArray groupKeyJsonArray = new JSONArray();
		for (String[] singleKeyArray: keys)
		{
			JSONArray singleKeyJsonArray = new JSONArray();
			
			for (Object keyString:singleKeyArray) {
				singleKeyJsonArray.put(keyString);
				
				keyStringLengthCount += singleKeyJsonArray.toString().length();
			}
			
			if(keyStringLengthCount>queryMaxLength) {
				queryStringArray.add(groupKeyJsonArray.toString()); // store for request first
				
				keyStringLengthCount = 0; //reset
				groupKeyJsonArray = new JSONArray(); //reset
				
				groupKeyJsonArray.put(singleKeyJsonArray); //to be next request
			}else {
				groupKeyJsonArray.put(singleKeyJsonArray);
			}
		}
		queryStringArray.add(groupKeyJsonArray.toString()); // store the last record
		
		//Handle query data to Couchbase
		JSONArray resultJsonArray = new JSONArray();
		
		for(String query : queryStringArray) {
			Log.debug("*** getDocsByView: "+viewName+"="+query);
			
			//Get data from Couchbase
			try {
				String url = "_design/main/_view/"+viewName+"?stale=false&keys="+ URLEncoder.encode(query, "UTF-8");
				Log.debug("*** url="+url);
				JSONObject resultFromCouchBase = getDoc(url);
				Log.debug("*** resultFromCouchBase="+resultFromCouchBase);
				
				if(resultFromCouchBase != null) {
					if(resultFromCouchBase.has("rows")) {
						JSONArray rowsJsonArray = resultFromCouchBase.getJSONArray("rows");
						
						for(Object item : rowsJsonArray) {
							resultJsonArray.put(item); //append JSON object
						}
					}
				}
			}catch (Exception ex) {
				Log.error(ex);
			}
		}
		
		return resultJsonArray; 
	}
	
}