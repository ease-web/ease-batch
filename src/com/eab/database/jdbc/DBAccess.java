package com.eab.database.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.eab.common.Constant;
import com.eab.common.Log;

public class DBAccess {

	protected java.sql.Connection conn = null;
	static protected javax.sql.DataSource datasource = null;
	
	static public void setDatasource(javax.sql.DataSource ds) {
		if (ds == null) {
			Log.info("[DBAccess] Datasrouce is empty.  Please check.");
		}
		datasource = ds;
	}
	
	static public Connection getConnection() throws SQLException {
		Connection conn = null;
		Context context = null;
		
		// Check Datasource
		try {
			conn = datasource.getConnection();
			if (conn == null || conn.isClosed()) {
				Log.info("[DBAccess] Reload Datasrouce: " + Constant.DATASOURCE_STRING);
				try {
					context = new InitialContext();
					setDatasource((DataSource) context.lookup(Constant.DATASOURCE_STRING));		// Renew Datasource
				} catch (NamingException e) {
					Log.error(e);
				}
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
				conn = null;
			} catch (SQLException e) {
				Log.error(e);
			}
		}
		
		return datasource.getConnection();
	}
}