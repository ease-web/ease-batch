package com.eab.servlet.manual;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.EnvVariable;
import com.eab.common.Log;

/**
 * Servlet implementation class WS_SetExpiredApprovalCase
 */
@WebServlet("/manual/SendHealthDeclarationNotification")
public class WS_SendHealthDeclaration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WS_SendHealthDeclaration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if ("YES".equals(EnvVariable.get("ENABLE_MANUAL_BATCH"))) {
			ExecutorService executor = Executors.newSingleThreadExecutor();
			executor.submit(() -> {
				try {
					com.eab.batch.approvalnotification.SendHealthDeclarationNotification.startBatch();
				} catch(Exception e) {
					Log.error(e);
				}
			});
			response.getWriter().append("Fired: SendHealthDeclarationNotification.startBatch()");
		} else {
			response.getWriter().append("Disabled");
		}
	}

}
