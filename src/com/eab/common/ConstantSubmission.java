package com.eab.common;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.SessionFactory;

import com.eab.couchbase.CBUtil;

public class ConstantSubmission {
	public static String EAPP_FN = "appPdf";
	public static String BI_FN = "proposal";
	public static String FNAREPORT_FN = "fnaReport";
	public static String PADDR_PROOF = "pAddrProof";
	public static String SUPERVISOR_EAPPROVAL = "eapproval_supervisor_pdf";
	public static String FAFIRM_COMMENT = "faFirm_Comment";
	public static String PNRIC = "pNric";
	public static String PPASS = "pPass";
	public static String IADDR_PROOF = "iAddrProof";
	public static String INRIC = "iNric";
	public static String IPASS = "iPass";
	
	private static final Map<String, String> titleMap;
	private static final Map<String, String> typeMap;

	static {
		titleMap = new HashMap<String, String>();
	    String[][] pairs = {
	        {EAPP_FN, "Proposal Form"},
	        {BI_FN, "Benefit Illustration/Product Summary"},
	        {FNAREPORT_FN, "My Financial Profile"},
	        {PNRIC, "NRIC/Birth certificate/Employment pass"},
	        {PPASS, "NRIC/Birth certificate/Employment pass"},
	        {PADDR_PROOF, "NRIC/Birth certificate/Employment pass"},
	        {INRIC, "NRIC/Birth certificate/Employment pass"},
	        {IPASS, "NRIC/Birth certificate/Employment pass"},
	        {IADDR_PROOF, "NRIC/Birth certificate/Employment pass"}
	    };
	    for (String[] pair : pairs) {
	    	getTitleMap().put(pair[0], pair[1]);
	    }
	    
	    typeMap = new HashMap<String, String>();
	    String[][] typePairs = {
	        {EAPP_FN, "NWBSG1PROP"},
	        {BI_FN, "NWBSG1BENIL"},
	        {FNAREPORT_FN, "NWBSG1MFINP"},
	        {PNRIC, "NWBSG1ID"},
	        {PPASS, "NWBSG1ID"},
	        {PADDR_PROOF, "NWBSG1ID"},
	        {INRIC, "NWBSG1ID"},
	        {IPASS, "NWBSG1ID"},
	        {IADDR_PROOF, "NWBSG1ID"}
	    };
	    for (String[] pair : typePairs) {
	    	getTypeMap().put(pair[0], pair[1]);
	    }
	}

	public static Map<String, String> getTitleMap() {
		return titleMap;
	}
	
	public static Map<String, String> getTypeMap() {
		return typeMap;
	}
}
