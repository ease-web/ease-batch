package com.eab.common;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class HttpUtil {
	private static Response sendHttpRequest(HttpRequestBase request, String proxyUrl) throws ClientProtocolException, IOException {
		ResponseBuilder responseBuilder = null;
		
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();	//Change to createAcceptSelfSignedCertClient() if going to support Self Signed Cert.
		if (proxyUrl != null && StringUtils.isNotEmpty(proxyUrl)) {
			request.setConfig(RequestConfig.custom().setProxy(HttpHost.create(proxyUrl)).build());
		}
		CloseableHttpResponse response = httpclient.execute(request);
		String responseString = (response.getEntity()!=null)?EntityUtils.toString(response.getEntity()):"";
		
//		Response result = Response.status(response.getStatusLine().getStatusCode()).entity(responseString).build();
		Header[] responseHeader = response.getAllHeaders();
		responseBuilder = Response.status(response.getStatusLine().getStatusCode());
		responseBuilder.entity(responseString);
		for(Header header : responseHeader) {
			responseBuilder.header(header.getName(), header.getValue());
		}
		Response result = responseBuilder.build();
		
		response.close();
		httpclient.close();
		
		stopWatch.stop();
		Log.info("sendHttpRequest | " + request.getURI().toString() + " | Elapsed time: " + stopWatch.getTime() + " mills");
		stopWatch = null;

		return result;
	}
	
	public static Response send(HttpRequestBase request) throws ClientProtocolException, IOException {
		String host = request.getURI().getHost();
		Gson gson = new Gson();
		boolean isInt = false;
		boolean isExt = false;
		JsonElement jelem = null;
		JsonObject jobj = null;
		Type listType = new TypeToken<List<String>>(){}.getType();
		List<String> hostList = null;
		
		String strExt = EnvVariable.get("INTERNET_PROXY_HOSTS");	//External
		String strInt = EnvVariable.get("INTRANET_PROXY_HOSTS");	//Internal
		
		//Scan External White List
		if (!isInt & !isExt) {
			if (strExt != null && strExt.length() > 0) {
				Log.debug("Check Internet Proxy --<");
				jelem = gson.fromJson(strExt, JsonElement.class);
				if (jelem != null) {
					jobj = jelem.getAsJsonObject();
					hostList = new Gson().fromJson(jobj.get("hostname"), listType);
					
					for (String hostItem : hostList) {
						Log.debug("Ext: " + host + " <--> " + hostItem);
						if (hostItem.equalsIgnoreCase(host)) {
							isExt = true;
							Log.debug("Matched!");
							break;
						}
					}
				}
			}
		}
		
		//Scan Internal White List
		if (!isInt & !isExt) {
			if (strInt != null && strInt.length() > 0) {
				Log.debug("Check Intranet Proxy --<");
				jelem = gson.fromJson(strInt, JsonElement.class);
				if (jelem != null) {
					jobj = jelem.getAsJsonObject();
					hostList = new Gson().fromJson(jobj.get("hostname"), listType);
					
					for (String hostItem : hostList) {
						Log.debug("Intt: " + host + " <--> " + hostItem);
						if (hostItem.equalsIgnoreCase(host)) {
							isInt = true;
							Log.debug("Matched!");
							break;
						}
					}
				}
			}
		}
		
		if (isExt) {
			Log.debug("Goto INTERNET_PROXY");
			if (EnvVariable.get("INTERNET_PROXY") != null && EnvVariable.get("INTERNET_PROXY").length() > 0)
				return sendHttpRequest(request, EnvVariable.get("INTERNET_PROXY"));
		} else if (isInt) {
			Log.debug("Goto INTRANET_PROXY");
			if (EnvVariable.get("INTRANET_PROXY") != null && EnvVariable.get("INTRANET_PROXY").length() > 0)
				return sendHttpRequest(request, EnvVariable.get("INTRANET_PROXY"));
		}

		Log.debug("Goto NO_PROXY");
		return sendHttpRequest(request, null);
	}
	
	public static String convertHeaderToStr(Header[] headers) {
		String output = "";
		
		for(Header header : headers) {
			output += header.getName() + ": ";
			output += (header.getValue() == null?"":header.getValue()) + "\n";
		}
		
		return output;
	}
	
	private static CloseableHttpClient createAcceptSelfSignedCertClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		// use the TrustSelfSignedStrategy to allow Self Signed Certificates
		SSLContext sslContext = SSLContextBuilder
				.create()
				.loadTrustMaterial(new TrustSelfSignedStrategy())
				.build();
		
		// we can optionally disable hostname verification. 
		// if you don't want to further weaken the security, you don't have to include this.
		HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
		
		// create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
		// and allow all hosts verifier.
		SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
		
		// finally create the HttpClient using HttpClient factory methods and assign the ssl socket factory
		return HttpClients
				.custom()
				.setSSLSocketFactory(connectionFactory)
				.build();
	}
}
