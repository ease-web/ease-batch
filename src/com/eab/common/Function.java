package com.eab.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.swing.ImageIcon;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.dom4j.DocumentException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.object.AgentProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class Function {
	private static final float PDF_HEIGHT = 1200;
	private static final float PDF_WIDTH = 800;
	
	public static String GenerateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return new Base64().encodeAsString(bytes);
	}
	
	public static String GenerateRandomASCII(int length) {
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		// Random rng = new Random();
		java.security.SecureRandom rng = new java.security.SecureRandom();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}
	
	public static String ByteToStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes)
			sb.append((char) b);
		return sb.toString();
	}

	public static byte[] StrToByte(String text) {
		return text.getBytes();
	}

	public static String ByteToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToByte(String s) {
		return new BigInteger(s, 16).toByteArray();
	}

	public static byte[] StrToBase64(String text) {
		return text.getBytes();
	}

	public static String Base64ToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}
	
	public static String ByteToBase64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	public static byte[] HexToStr(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return data;
	}

	private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static String StrToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	public static String getClientIp(HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}
		return remoteAddr;
	}
	
	public static String getServerIp() {
    	String ipAddr = "0.0.0.0";
    	
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
                Enumeration<InetAddress> nias = ni.getInetAddresses();
                while(nias.hasMoreElements()) {
                    InetAddress ia= (InetAddress) nias.nextElement();
                    if (!ia.isLinkLocalAddress() 
                     && !ia.isLoopbackAddress()
                     && ia instanceof Inet4Address) {
                    	return ia.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
        	Log.error(e);
        }
        return ipAddr;
    }
	
	public static String getServerIp(HttpServletRequest request) {
		String ipAddr = (request != null) ? request.getLocalAddr() : "0.0.0.0";
		return ipAddr;
	}
	
	public static String getBaseUrl(HttpServletRequest request) {
		return (request.getRequestURL()).substring(0, (request.getRequestURL()).length()
				- (request.getRequestURI()).length() + (request.getContextPath()).length()) + "/";
	}

	public static String getPathUrl(HttpServletRequest request) {
		return request.getServletPath();
	}
	
	public static ZonedDateTime getDateUTCNow() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static final DateTimeFormatter DATE_FORMAT_ISO8601 = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_ISO8601);
	public static final DateTimeFormatter DATE_FORMAT_AXA = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_AXA);

	public static String getDateUTCNowStr() {
		return DATE_FORMAT_ISO8601.format(getDateUTCNow());
	}
	
	public static boolean dateFormatValidator(String value, String format) {
		try {
			new SimpleDateFormat(format).parse(value);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String convertDateTimeZoneString(String fromDateStringAsISO8601, String targetTimeZoneId,
			DateTimeFormatter format) {

		// Convert to target time zone.
		// For example, targetTimeZoneId is Singapore, then 2017-09-03T09:46:07Z to be
		// 2017-09-03 17:46:07
		ZonedDateTime currentDateTime = ZonedDateTime.parse(fromDateStringAsISO8601, DATE_FORMAT_ISO8601);
		ZonedDateTime convertedDateTime = currentDateTime.withZoneSameInstant(ZoneId.of(targetTimeZoneId));
		String convertedDateString = format.format(convertedDateTime);

		Log.debug("*** fromDateStringAsISO8601=" + fromDateStringAsISO8601 + ", targetTimeZoneId=" + targetTimeZoneId
				+ ", convertedDateString=" + convertedDateString);

		return convertedDateString;
	}

	public static String convertDateString(String value, String fromFormat, String toFormat) throws ParseException {

		String convertedDateString = "";

		SimpleDateFormat fromDateFormatter = new SimpleDateFormat(fromFormat);

		java.util.Date date = fromDateFormatter.parse(value);

		SimpleDateFormat toDateFormatter = new SimpleDateFormat(toFormat);
		
		convertedDateString = toDateFormatter.format(date);

		return convertedDateString;
	}
	
	public static JsonObject validateJsonFormatForString(String jsonString) {
		JsonObject jsonBody = null;
		
		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonObject();

		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return jsonBody;
	}
	
	public static String extractException(Exception e) {
		String message = e.toString() + "\n";
		StackTraceElement[] trace = e.getStackTrace();
		for (int i = 0; i < trace.length; i++) {
			message += trace[i].toString() + "\n";
		}
		
		return message;
	}
	
	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}
	
	public static java.sql.Date covertZonedDateTimeToSqlDate(ZonedDateTime zDate) {
		return new java.sql.Date(java.util.Date.from(zDate.toInstant()).getTime());
	}
	
	public static java.sql.Date sqlDateFromString(String value, String dateFormat) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		java.util.Date parsed = format.parse(value);
		
		return convertJavaDateToSqlDate(parsed);
	}
	
	public static boolean isNumeric(String text) {
		boolean result = false;
		try {
			Integer.parseInt(text);
			result = true;
		} catch(Exception e) {
			result = false;
		}
		
		return result;
	}
	
	public static boolean isASCII(String text) {
		boolean result = false;
		if (text != null)
			result = text.matches("\\p{ASCII}*");
		
		return result;
	}
	
	public boolean handleResponse(String resp, String respObjkey) throws Exception {
		Log.debug("Start to handle Response");
		
		JsonParser jsonParser = new JsonParser();
		JsonObject respObj = (JsonObject)jsonParser.parse(resp);
		JsonObject exceptObj;
		String exceptMsg;
		String exceptReason;
		String exceptCode;
		String successFlag;
		
		JsonObject successObj = respObj.getAsJsonObject(respObjkey);
		if (respObj.entrySet().size() == 0) {
			Log.debug("Response Object is Empty, Job Success");
			return true;
		}
		else if (successObj != null ){
			successFlag = getStringFromJsonObject(successObj.get("responseFlag"));
			Log.debug("Print Success Obj");
			Log.debug(successFlag);
			return true;
		} else {
			exceptObj = respObj.getAsJsonObject("exception");
			if (exceptObj != null) {
				exceptMsg = getStringFromJsonObject(exceptObj.get("message"));
				exceptReason = getStringFromJsonObject(exceptObj.get("reason"));
				exceptCode = getStringFromJsonObject(exceptObj.get("reasonCode"));
				Log.debug("Print Failure obj");
				Log.debug(exceptCode);
				Log.debug(exceptReason);
				Log.debug(exceptMsg);
			}
			return false;
		}
	}
	
	public String getStringFromJsonObject(JsonElement ele){
		if (ele != null) {
			return ele.getAsString();
		} else return "";
	}

    public ArrayList<String> getContentToSend(ArrayList<String> base64Strs, ArrayList<String> fileType) throws Exception {
    	//ByteArrayOutputStream os = new ByteArrayOutputStream();
    	ArrayList<String> contentTosend = new ArrayList<String>();
        for (int i = 0; i < base64Strs.size(); i++) {
        	if (fileType.get(i).contains("image")) {
        		contentTosend.add(convertImageToPdfBase64Str(base64Strs.get(i), i));
        	} else if (fileType.get(i).contains("pdf")) {
        		contentTosend.add(base64Strs.get(i));
        	}	
        }
       
    	return contentTosend;
    }
    
	private static boolean isEncryptedPdf(String docSrc) throws Exception {
    	byte[] encodedByte = Base64.decodeBase64(docSrc);
    	InputStream is = null;
    	PDDocument document = null;
    	boolean isProtected;
    	
    	try{
    		is = new ByteArrayInputStream(encodedByte);
    		document = PDDocument.load(is);
    		isProtected = false;
    	} catch (Exception e) {
    		isProtected = true;
    	} finally {
    		if(is != null)
    			is.close();
    		if(document != null)
    			document.close();
    	}

    	return isProtected;
    }
    
    private String mergePdf(ArrayList<String> Base64StrsArr) throws Exception {
    	byte[] encodedByte;
    	InputStream is;
    	ByteArrayOutputStream mergedPDFOutputStream = null;
    	PDFMergerUtility PDFmerger = new PDFMergerUtility(); 
    	List<InputStream> sources = new ArrayList<InputStream>();;
    	try {
    		for (int i = 0; i < Base64StrsArr.size(); i++) {
        		encodedByte = Base64.decodeBase64(Base64StrsArr.get(i));
        		is = new ByteArrayInputStream(encodedByte);
        		sources.add(is);
            }
        	PDFmerger.addSources(sources);
        	mergedPDFOutputStream = new ByteArrayOutputStream();
        	PDFmerger.setDestinationStream(mergedPDFOutputStream);
        	
        	PDFmerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
    	} finally {
    		for (InputStream source : sources) {
    			//IOUtils.closeQuietly(source);
    			if (source != null) {
    				source.close();
    			}
    		}
    		if (mergedPDFOutputStream != null) {
    			mergedPDFOutputStream.close();
    		}
    	}
    	
    	String base64 = new String(Base64.encodeBase64(mergedPDFOutputStream.toByteArray()));
    	
    	return base64;
    }
    
    private String convertImageToPdfBase64Str(String imgSrc, int index) throws Exception {
    	ByteArrayOutputStream byteArrayOutputStream = null;
    	byte[] encodedByte;
    	InputStream is;
    	PDDocument document = null;
    	PDPageContentStream contentStream = null;
    	String base64Str = "";
    	try {
    		byteArrayOutputStream = new ByteArrayOutputStream();
        	encodedByte = Base64.decodeBase64(imgSrc);
        	is = new ByteArrayInputStream(encodedByte);
        	
        	document = new PDDocument();
        	PDPage pageAdded = new PDPage();
        	document.addPage(pageAdded);
        	PDPage page = document.getPage(0);
        	BufferedImage pdImage = ImageIO.read(is);
        	PDImageXObject obj =  JPEGFactory.createFromImage(document, pdImage);
        	
        	contentStream = new PDPageContentStream(document, page);
        	
        	float scale = getImgScale(page, obj, 40);
        	contentStream.drawImage(obj, 40f, page.getMediaBox().getHeight() - obj.getHeight() * scale - 40, obj.getWidth() * scale, obj.getHeight() * scale);
        	contentStream.close();
        	
        	document.save(byteArrayOutputStream);
        	base64Str = new String(Base64.encodeBase64(byteArrayOutputStream.toByteArray()));
    	} finally {
    		if (document != null)
    			document.close();
    		if (byteArrayOutputStream != null)
    			byteArrayOutputStream.close();	
    	}

    	return base64Str;
    	
    }
    
    private float getImgScale(PDPage page, PDImageXObject img, int margin) {
		float scale = 1;
		float availableWidth = page.getMediaBox().getWidth() - margin * 2;
		if (img.getWidth() > availableWidth) {
			scale = Math.min(scale, availableWidth / img.getWidth());
		}
		float availableHeight = page.getMediaBox().getHeight() - margin * 2;
		if (img.getHeight() > availableHeight) {
			scale = Math.min(scale, availableHeight / img.getHeight());
		}
		return scale;
	}
    
    public HashMap<String, String>  getAgentProfileInfo(String agentCode){
    	HashMap<String, String> profile = new HashMap<String, String>();
    	JSONObject viewRows = Constant.CBUTIL.getRecordsByView("agentWithDescendingOrder", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",99999505729340000]", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",0]&descending=true");
    	
    	JSONArray rows = viewRows.getJSONArray("rows");
    	JSONObject valueObj;
    	String docId;
    	if (rows.length() > 0){
    		valueObj = rows.getJSONObject(0).getJSONObject("value");
    		docId = rows.getJSONObject(0).getString("id");
			JsonObject doc = transformObject(Constant.CBUTIL.getDoc(docId));
			profile.put("email", transformEleToString(doc.get("email")));
			profile.put("mobile", transformEleToString(doc.get("mobile")));
			profile.put("name", transformEleToString(doc.get("name")));
			profile.put("managerCode", transformEleToString(doc.get("managerCode")));
			return profile;
    	}else {
    		return profile;
    	}
    }
    
    public AgentProfile getAgentProfile(String agentCode){
    	AgentProfile profile = null;
    	JSONObject viewRows = Constant.CBUTIL.getRecordsByView("agentWithDescendingOrder", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",99999505729340000]", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",0]&descending=true");
    	
    	JSONArray rows = viewRows.getJSONArray("rows");
    	JSONObject valueObj;
    	String managerCode, name, mobile, email;
    	boolean isProxying;
    	if (rows.length() > 0){
    		valueObj = rows.getJSONObject(0).getJSONObject("value");
    		
    		managerCode = valueObj.has("managerCode") ? valueObj.getString("managerCode") : "";
    		name = valueObj.has("agentName") ? valueObj.getString("agentName") : "";
    		mobile = valueObj.has("mobile") ? valueObj.getString("mobile") : "";
    		email = valueObj.has("email") ? valueObj.getString("email") : "";
    		isProxying = valueObj.has("isProxying") ? valueObj.getBoolean("isProxying") : false;
			
			profile = new AgentProfile(agentCode, managerCode, name, mobile, email, isProxying);
			return profile;
    	}else {
    		return profile;
    	}
    }
    
    public HashMap<String, String> getFAAdminProfileByAgentCode(String agentCode) {
    	HashMap<String, String> profile = new HashMap<String, String>();
    	JSONObject viewRows = Constant.CBUTIL.getRecordsByView("agentWithDescendingOrder", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",99999505729340000]", "[\"01\",\"agentCodeFirst\",\"" + agentCode + "\",0]&descending=true");
    	
    	JSONArray rows = viewRows.getJSONArray("rows");
    	JsonObject valueObj;
    	String docId;
    	String faAdminCode;
    	if (rows.length() > 0){
    		valueObj = transformObject(rows.getJSONObject(0).getJSONObject("value"));
    		faAdminCode = transformEleToString(valueObj.get("faAdminCode"));
    		profile = getAgentProfileInfo(faAdminCode);
			return profile;
    	}else {
    		return profile;
    	}
    }
    
    public JsonObject transformObject(JSONObject obj) {
    	if (obj == null) {
    		return null;
    	}
		JsonParser jsonParser = new JsonParser();
		JsonObject transformedObj;
		transformedObj = (JsonObject)jsonParser.parse(obj.toString());
		return transformedObj;
    }
    
    public String transformEleToString(JsonElement ele) {
    	if (ele == null ){
    		return "";
    	} else {
    		return ele.getAsString();
    	}
    }
    
    public boolean transformEleToBoolean(JsonElement ele) {
    	if (ele == null ){
    		return false;
    	} else {
    		return ele.getAsBoolean();
    	}
    }
    
    public JsonArray transformEleToArray(JsonElement ele) {
    	if (ele == null ){
    		return new JsonArray();
    	} else {
    		return ele.getAsJsonArray();
    	}
    }
    
    public JsonObject transformEleToObj(JsonElement ele) {
    	if (ele == null ){
    		return new JsonObject();
    	} else {
    		return ele.getAsJsonObject();
    	}
    }
    
    public JsonElement getAsPath(JsonElement e, String path){
    	JsonElement current = e;
        String ss[] = path.split("/");
        for (int i = 0; i < ss.length; i++) {
        	if (current != null) {
        		current = current.getAsJsonObject().get(ss[i]);
        	}
        }
        return current;
    }
    
	public static String getServerName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			Log.error(e);
		}
		return "";
	}
	
	public static long getLongStartDateFilter(int offsetDays) throws ParseException{
		ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID)).plusDays(offsetDays);
		String dateStr = currentTime.getYear() + "-" + String.format("%02d", currentTime.getMonthValue()) + "-" + String.format("%02d", currentTime.getDayOfMonth()) + "000000000";
		LocalDateTime ldt = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-ddHHmmssSSS"));
		
		ZonedDateTime singaporeZonedDateTime = ldt.atZone(ZoneId.of(Constant.ZONE_ID));
		ZoneId utcZoneId = ZoneId.of("UTC");
        ZonedDateTime utcDateStartTime = singaporeZonedDateTime.withZoneSameInstant(utcZoneId);
        
        return utcDateStartTime.toInstant().toEpochMilli();
	}
	
	public static long getLongEndDateFilter(int offsetDays) throws ParseException{
		ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID)).plusDays(offsetDays);
		String dateStr = currentTime.getYear() + "-" + String.format("%02d", currentTime.getMonthValue()) + "-" + String.format("%02d", currentTime.getDayOfMonth()) + "235959999";
		LocalDateTime ldt = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-ddHHmmssSSS"));
		
		ZonedDateTime singaporeZonedDateTime = ldt.atZone(ZoneId.of(Constant.ZONE_ID));
		ZoneId utcZoneId = ZoneId.of("UTC");
        ZonedDateTime utcDateEndTime = singaporeZonedDateTime.withZoneSameInstant(utcZoneId);
        
        return utcDateEndTime.toInstant().toEpochMilli();
	}
	
	public static String getExpiryDateFrSubmitted(String ISOString, int daysOfExpiry, String displayFormatStr) throws Exception{
		String df;
		if (ISOString.length() > 10) {
			df = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
		}else {
			df = "yyyy-MM-dd";
		}
		Date dateValue = null;
		SimpleDateFormat sdf = new SimpleDateFormat(df);
		SimpleDateFormat displayFormat = new SimpleDateFormat(displayFormatStr);
		dateValue = sdf.parse(ISOString);
		Calendar c = Calendar.getInstance();
		c.setTime(dateValue);
		c.add(Calendar.DATE, daysOfExpiry); 
		return displayFormat.format(c.getTime());
	}
	
	 public static Calendar convertISO8601(long timestamp) {
        ZonedDateTime utc = Instant.ofEpochMilli(timestamp ).atZone(ZoneOffset.UTC);
        return GregorianCalendar.from(utc);
	 }
	 
	 public static Calendar convertISO8601(String dateStr) {
	        return javax.xml.bind.DatatypeConverter.parseDateTime(dateStr);
	 }
	 
	 public static void changeTimezone(Calendar cal, String timezone) {
	        cal.setTimeZone(TimeZone.getTimeZone(timezone));
	 }
	 
	 public static String cal2StrFull(Calendar cal) {
	     	return cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DATE)
	     	        +" "+cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":"+cal.get(Calendar.MILLISECOND)+" "+cal.getTimeZone().getDisplayName();
     }
     
     public static String cal2Str(Calendar cal) {
     		
     	int year = cal.get(Calendar.YEAR);
     	int month = cal.get(Calendar.MONTH)+1;
     	int date = cal.get(Calendar.DATE);
     	int hour = cal.get(Calendar.HOUR_OF_DAY);
     	int min = cal.get(Calendar.MINUTE); 
     	
         return year+"-"+(month<10?"0":"")+month+"-"+(date<10?"0":"") +date+" "+( hour<10?"0":"")+hour+":"+(min<10?"0":"") +min;
     }
     
     public static String callpostApi(JsonObject params, String path) throws Exception{
 		HttpClient httpclient = HttpClients.createDefault();
 		
 		HttpPost httppost = new HttpPost(path);
 		httppost.setHeader("Content-Type", "application/json");
 		
 		httppost.setEntity(new StringEntity(params.toString(), StandardCharsets.UTF_8.name()));

 		//Execute and get the response.
 		HttpResponse response = httpclient.execute(httppost);
 		
 		//response.getStatusLine().getStatusCode();

 		HttpEntity entity = response.getEntity();

 		if (entity != null) {
 			String responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
 			return  responseString;
 		} else {
 			return "";
 		}
 	}
     
     public static String callGetApi(String path) throws Exception{
  		HttpClient httpclient = HttpClients.createDefault();
  		
  		HttpGet httpget = new HttpGet(path);
  		httpget.setHeader("Content-Type", "application/json");

  		//Execute and get the response.
  		HttpResponse response = httpclient.execute(httpget);
  		
  		//response.getStatusLine().getStatusCode();

  		HttpEntity entity = response.getEntity();

  		if (entity != null) {
  			String responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
  			return  responseString;
  		} else {
  			return "";
  		}
  	}
 	
}
