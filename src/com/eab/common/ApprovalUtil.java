package com.eab.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ApprovalUtil {
	
	public static HashMap<String, JsonObject> getRelatedAgentsWithRoleAssigned(String compCode, String agentCode, HashMap<String, String> approvalInfo, JsonObject allAgents, JsonObject mappingUserIdtoAgentCode, int secProxyDays){
		HashMap<String, JsonObject> result = new HashMap<String, JsonObject>();
		try {
			JsonObject agent = (allAgents.get(agentCode) != null) ? allAgents.get(agentCode).getAsJsonObject() : new JsonObject();
			
			String managerCode = (agent.get("managerCode") != null) ? agent.get("managerCode").getAsString() : null;
			JsonObject manager = (managerCode != null && allAgents.get(managerCode) != null) ? allAgents.get(managerCode).getAsJsonObject() : new JsonObject();
			
			String directorCode = (manager.get("managerCode") != null) ? manager.get("managerCode").getAsString() : null;
			JsonObject director = (directorCode != null && allAgents.get(directorCode) != null) ? allAgents.get(directorCode).getAsJsonObject() : new JsonObject();
			
			JsonObject proxy1 = getProxyProfile(1, manager, allAgents, mappingUserIdtoAgentCode);
			
			JsonObject proxy2 = getProxyProfile(2, manager, allAgents, mappingUserIdtoAgentCode);
			
			if (secProxyDays < 0) {
				secProxyDays = secProxyDays * -1;
			}
			
			JsonObject assignedManager = getAssignedProfile(approvalInfo, agent, manager, director, proxy1, proxy2, secProxyDays);
			
			result.put("agent", agent);
			result.put("manager", manager);
			result.put("director", director);
			result.put("proxy1", proxy1);
			result.put("proxy2", proxy2);
			result.put("assignedManager", assignedManager);
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
	}
	
	private static JsonObject getProxyProfile(int proxyType, JsonObject manager, JsonObject allAgents, JsonObject mappingUserIdtoAgentCode){
		JsonObject result = new JsonObject();
		try {
			String searchKey = "";
			searchKey = (proxyType == 1) ? "proxy1UserId" : "proxy2UserId";
			
			if(manager != null && manager.get("rawData") != null && manager.get("rawData").getAsJsonObject().get(searchKey) !=null) {
				Function func = new Function();
				String searchAgentUserId = manager.get("rawData").getAsJsonObject().get(searchKey).getAsString();
				String searchAgentCode = func.transformEleToString(mappingUserIdtoAgentCode.get(searchAgentUserId));
				result = (allAgents.get(searchAgentCode) != null) ? allAgents.get(searchAgentCode).getAsJsonObject() : new JsonObject();
			}
			
			return result;
		} catch (Exception e){
			e.printStackTrace();
			return result;
		}
		
	}
	
	public static String getProxySupervisorCode(String managerCode) {
		return managerCode;
	}
	
	public static String getCSVString(JsonArray jArrList) {
		String result = "";
		try {
			List<String> arrList = new ArrayList<String>();
			for (int i=0; i<jArrList.size(); i++) {
				arrList.add(jArrList.get(i).getAsString());
			}
			result = String.join(",", arrList); 
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
	}
	
	public static HashMap<String, JsonObject> getAssignedProfileBySubmissionViewObj(JsonObject submissionViewRowsValue, int secAssignmentDay){
		JsonObject agentsWithProxyAssignment = AgentUtil.geAllAgents("01");
		JsonObject mappingUserIdtoAgentCode = AgentUtil.getRawDataAgentsByUserId("01");
		Function func = new Function(); 
		HashMap<String, String> fallInCase = new HashMap<String, String>();
		fallInCase  = new HashMap<String, String>();
		fallInCase.put("submittedDate", func.transformEleToString(submissionViewRowsValue.get("submittedDate")));
		fallInCase.put("agentId", func.transformEleToString(submissionViewRowsValue.get("agentId")));
		fallInCase.put("approvalStatus", func.transformEleToString(submissionViewRowsValue.get("status")));
		fallInCase.put("caseLockedManagerCodebyStatus", func.transformEleToString(submissionViewRowsValue.get("caseLockedManagerCodebyStatus")));
		
		return ApprovalUtil.getRelatedAgentsWithRoleAssigned("01", func.transformEleToString(submissionViewRowsValue.get("agentId")), fallInCase, agentsWithProxyAssignment, mappingUserIdtoAgentCode, secAssignmentDay);
	}
	
	private static JsonObject getAssignedProfile(HashMap<String, String>approvalInfo, JsonObject agent, JsonObject manager, JsonObject director, JsonObject proxy1, JsonObject proxy2, int secProxyDays){
		JsonObject result = new JsonObject();
		try {
			String dateStr = approvalInfo.get("submittedDate");
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			date = format.parse(dateStr);
			ZonedDateTime submittedTime = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of(Constant.ZONE_ID));
			ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID));
			
			boolean managerIsProxying = (manager.get("isProxying") != null) ? manager.get("isProxying").getAsBoolean() : false;
			boolean proxy1IsProxying = (proxy1.get("isProxying") != null) ? proxy1.get("isProxying").getAsBoolean() : false;
			boolean proxy2IsProxying = (proxy2.get("isProxying") != null) ? proxy2.get("isProxying").getAsBoolean() : false;
			
			String agentCode = (agent.get("agentCode") != null) ? agent.get("agentCode").getAsString() : "a";
			String managerCode = (manager.get("agentCode") != null) ? manager.get("agentCode").getAsString() : "m";
			String proxy1Code = (proxy1.get("agentCode") != null) ? proxy1.get("agentCode").getAsString() : "p1";
			String proxy2Code = (proxy2.get("agentCode") != null) ? proxy2.get("agentCode").getAsString() : "p2";
			String caseLockedManagerCodebyStatus = (approvalInfo.get("caseLockedManagerCodebyStatus") != null )  ? approvalInfo.get("caseLockedManagerCodebyStatus") : "";
						
			if(
				manager != null && (managerIsProxying == false ||
				((proxy1 == null || proxy1.isJsonNull() || proxy1IsProxying == true) && (proxy2 == null || proxy2.isJsonNull() || proxy2IsProxying == true)) ) 
					){
				result = manager;
			} else if (
				proxy1 != null && agent	!= null && !agentCode.equalsIgnoreCase(proxy1Code)
				 && proxy1IsProxying == false 
				 && ((approvalInfo.get("approvalStatus").equalsIgnoreCase("SUBMITTED") || approvalInfo.get("approvalStatus").equalsIgnoreCase("E")) &&
						 ChronoUnit.DAYS.between(submittedTime, currentTime) < secProxyDays || proxy2 == null || (proxy2 != null && proxy2IsProxying == true) ||
								 ( (approvalInfo.get("approvalStatus").equalsIgnoreCase("PDoc") || approvalInfo.get("approvalStatus").equalsIgnoreCase("PDis")) && caseLockedManagerCodebyStatus.equalsIgnoreCase(proxy1Code))
						 )
					){
				result = proxy1;
			} else if (
				proxy2 != null && agent	!= null && !agentCode.equalsIgnoreCase(proxy2Code)
				 && proxy2IsProxying == false 
				 && ((approvalInfo.get("approvalStatus").equalsIgnoreCase("SUBMITTED") || approvalInfo.get("approvalStatus").equalsIgnoreCase("E")) &&
						 ChronoUnit.DAYS.between(submittedTime, currentTime) >= secProxyDays ||
						 ( (approvalInfo.get("approvalStatus").equalsIgnoreCase("PDoc") || approvalInfo.get("approvalStatus").equalsIgnoreCase("PDis")) && caseLockedManagerCodebyStatus.equalsIgnoreCase(proxy2Code)) ||
						 (proxy1IsProxying == true || agentCode.equalsIgnoreCase(proxy1Code))
						 )	
					){
				result = proxy2;
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
	}
	
	public static String getApprovalIdFrApplicationId(String appId) {
		return "SP" + appId.substring(2, 14);
	}
	
}


