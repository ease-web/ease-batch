package com.eab.batch.payment;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpGet;
import org.bouncycastle.asn1.ASN1ApplicationSpecific;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.API_PAYMENT_TRX;
import com.eab.dao.BATCH_CONFIRM_PAID_TRX;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.BATCH_SIGN_EXPIRY_TRX;
import com.google.gson.JsonObject;

public class confirmApplicationPaidToRLS implements Job {
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");

		stopWatch = null;
	}

	public void startBatch() {
		Log.info("Start confirmApplicationPaidToRLS");

		try {
			int bNo = 0;
			BATCH_LOG bLog = new BATCH_LOG();		
			bNo = bLog.selectSeq();
			if (Constant.CBUTIL == null) {
		        String gatewayUser = EnvVariable.get("GATEWAY_USER");
		        String gatewayPW = EnvVariable.get("GATEWAY_PW");

				Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			}
			
			// retrieve target signature date 
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("mobileInvalidatedPaidToRLS", "[\"01\",\"0\"]", "[\"01\",\"ZZZ\"]", "mobile");
			int totRecords = 0;
			if (viewRows != null && viewRows.has("rows")) {
				JSONArray rows = viewRows.getJSONArray("rows");				
				if (rows != null) {
					BATCH_CONFIRM_PAID_TRX bseDao = new BATCH_CONFIRM_PAID_TRX();
					boolean trxCreated;
					totRecords = rows.length();
					Function func = new Function();
					for (int i = 0; i < totRecords; i++) {
						try {
							JSONObject entry = rows.getJSONObject(i);
							JSONObject valueObj = entry.getJSONObject("value");
							String appId = valueObj.getString("appId");
							String polNo = valueObj.has("polNo")?valueObj.getString("polNo"):"";
							String bundleId = valueObj.has("bundleId")?valueObj.getString("bundleId"):"";						
							String payMethod = valueObj.has("payMethod")?valueObj.getString("payMethod"):"";
							String payStatus = valueObj.has("payStatus")?valueObj.getString("payStatus"):"";
							boolean isShield = valueObj.has("isShield") ? valueObj.getBoolean("isShield") : false;
							String parentId = valueObj.has("parentId")?valueObj.getString("parentId"):"";
							
							trxCreated = bseDao.create(appId, bNo, polNo, bundleId, payMethod+"("+payStatus+")");
							if (trxCreated) {
								JsonObject application = func.transformObject(Constant.CBUTIL.getDoc(appId));
								if (isShield) {
									JsonObject parentApplication = func.transformObject(Constant.CBUTIL.getDoc(parentId));
									if ((parentApplication.has("isInvalidatedPaidToRLS") && !parentApplication.get("isInvalidatedPaidToRLS").getAsBoolean())
											|| !parentApplication.has("isInvalidatedPaidToRLS")) {
										// Update the parent if it is not updated before
										parentApplication.addProperty("isInvalidatedPaidToRLS", true);
										Constant.CBUTIL.UpdateDoc(parentId, parentApplication.toString());
									}
								}
							
								JSONObject response = new JSONObject(Function.callGetApi(DOMAIN + "/submitInvalidApp/" + appId));
								if (response.has("success")) {
									application.addProperty("isInvalidatedPaidToRLS", true);
									Constant.CBUTIL.UpdateDoc(appId, application.toString());
								}
							} 
						}catch (Exception indExp) {
								Log.error(indExp);
						}
					}
				}
			}
		} catch (Exception exception) {
			Log.error(exception);
		}
	}
}