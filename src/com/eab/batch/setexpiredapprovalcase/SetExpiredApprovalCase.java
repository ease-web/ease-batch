package com.eab.batch.setexpiredapprovalcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.eab.dao.SUBMISSION_EXPIRED_CASE;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.exception.InvalidPropertiesException;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SetExpiredApprovalCase implements Job {
	private static final Set<String> ESCAPE_STATUS = new HashSet<String>(Arrays.asList(
		     new String[] {"A","R","E"}
	));
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}

	public static void startBatch() {
		Log.debug("SetExpiredApprovalCase Batch Job Start");
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_FOR_EAPPROVAL_SUBMITTED_EXPIRY_DATE");
			int controlValue = (tempValue == null) ? -14 : Integer.parseInt(tempValue);
			
			//String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			Log.debug("Start Range of Filter " + 0);
			Log.debug("End Range of Filter");
			Log.debug(endFilterStr);

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"expired\"," + 0 + ",\"0\"]", "[\"01\",\"expired\"," + endFilterStr + ",\"ZZZ\"]");
			if (viewRows == null) {
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");

			if (rows.length() == 0){
				return;
			}
			
			JsonObject valueObj;
			String status;
			String applicationId;
			String approvalCaseId;
			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();	
			HashMap<String, String> fallInCase = new HashMap<String, String>();
			
			Function func = new Function();
			
			for (int i = 0; i < rows.length(); i++){
				try {
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
					status = func.getStringFromJsonObject(valueObj.get("status"));
					applicationId = func.getStringFromJsonObject(valueObj.get("applicationId"));
					approvalCaseId = func.getStringFromJsonObject(valueObj.get("approvalCaseId"));

					if (!ESCAPE_STATUS.contains(status)) {
						fallInCase  = new HashMap<String, String>();
						fallInCase.put("applicationId", applicationId);
						fallInCase.put("approvalCaseId", approvalCaseId);
						
						list.add(fallInCase);
					}
					
				} catch (Exception e){
					Log.error(e);
				}
			};
			
			handleFallinCase(list);
			
			Log.debug("SetExpiredApprovalCase Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list){
		int bNo=0;
		try {
			if (list.size() > 0) {
				
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SETEXPIREDAPPROVALCASE", "P", new Date(), list.size());
				
				if (resultPreSub) {
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, String> fallInCase = (HashMap<String, String>) list.get(i);
						setApprovalDocExpired(fallInCase.get("approvalCaseId"), fallInCase.get("applicationId"), bNo);
					}

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static void setApprovalDocExpired(String docId, String appNo, int bNo) {
		try {
			JSONObject updateDoc = Constant.CBUTIL.getDoc(docId);
			Function func = new Function();
			JsonObject fallinCase = func.transformObject(updateDoc);
			if (updateDoc != null) {
				JsonParser jsonParser = new JsonParser();
				JsonObject transformedUpdateDoc;
				transformedUpdateDoc = (JsonObject)jsonParser.parse(updateDoc.toString());
				transformedUpdateDoc.addProperty("approvalStatus", "E");
				
				HashMap<String, String> agentProfile = new HashMap<String, String>();
				HashMap<String, String> managerProfile = new HashMap<String, String>();
				HashMap<String, String> directorProfile = new HashMap<String, String>();
				
				agentProfile = func.getAgentProfileInfo(func.transformEleToString(fallinCase.get("agentId")));
				managerProfile = func.getAgentProfileInfo(agentProfile.get("managerCode"));
				directorProfile = func.getAgentProfileInfo(managerProfile.get("managerCode"));

				transformedUpdateDoc.addProperty("directorId", managerProfile.get("managerCode"));
				transformedUpdateDoc.addProperty("directorName", directorProfile.get("name"));
				transformedUpdateDoc.addProperty("directorEmail", directorProfile.get("email"));
				transformedUpdateDoc.addProperty("directorMobile", directorProfile.get("mobile"));
				
				transformedUpdateDoc.addProperty("managerId", agentProfile.get("managerCode"));
				transformedUpdateDoc.addProperty("managerName", managerProfile.get("name"));
				transformedUpdateDoc.addProperty("managerEmail", managerProfile.get("email"));
				transformedUpdateDoc.addProperty("managerMobile", managerProfile.get("mobile"));
				
				TimeZone tz = TimeZone.getTimeZone("UTC");
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				df.setTimeZone(tz);
				String isoDate = df.format(new Date());
				transformedUpdateDoc.addProperty("expiredDate", isoDate);
				
				String result = Constant.CBUTIL.UpdateDoc(docId, transformedUpdateDoc.toString());
				
				if (result.equalsIgnoreCase("Update success")){
					SUBMISSION_EXPIRED_CASE subExpCase = new SUBMISSION_EXPIRED_CASE();
					subExpCase.create(bNo, docId, appNo);
				}
			}
		}catch (Exception e) {
			Log.error(e);
		}
	}
}
