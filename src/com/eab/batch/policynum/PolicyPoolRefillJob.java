package com.eab.batch.policynum;

import java.util.UUID;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.dao.POOL_NON_SHIELD;
import com.eab.dao.POOL_REFILL_TRX;
import com.eab.dao.POOL_SHIELD;

public class PolicyPoolRefillJob implements Job {

	private static final String POOL_SHIELD_SIZE_STRING = EnvVariable.get("POOL_SHIELD_SIZE");
	private static final int POOL_SHIELD_SIZE = Integer.parseInt(POOL_SHIELD_SIZE_STRING);

	private static final String POOL_NON_SHIELD_SIZE_STRING = EnvVariable.get("POOL_NON_SHIELD_SIZE");
	private static final int POOL_NON_SHIELD_SIZE = Integer.parseInt(POOL_NON_SHIELD_SIZE_STRING);

	private PolicyPoolRefillService policyPoolRefillService = new PolicyPoolRefillService();

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");

		stopWatch = null;
	}

	public void startBatch() {
		Log.info("Start PolicyPoolRefillJob");

		Log.debug("*********************        PolicyPoolRefillJob   START     ***********************");

		String uuid = UUID.randomUUID().toString();

		try {
			POOL_REFILL_TRX trxPoolRefillDao = new POOL_REFILL_TRX();
			boolean result = trxPoolRefillDao.create(uuid);
			Log.debug("***trxPoolRefillDao create=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}

		int requestShieldNum = 0;
		int requestNonShieldNum = 0;

		// Shield pool
		POOL_SHIELD shieldPoolDao = new POOL_SHIELD();
		int countShieldPool = POOL_SHIELD_SIZE;

		try {
			countShieldPool = shieldPoolDao.selectRowCount();

		} catch (Exception ex) {
			Log.error(ex);
		}

		int usedShieldNum = POOL_SHIELD_SIZE - countShieldPool;
		Log.debug("***** countShieldPool=" + countShieldPool + ", POOL_SHIELD_SIZE=" + POOL_SHIELD_SIZE
				+ ", usedShieldNum=" + usedShieldNum);

		if (usedShieldNum > 0) { // ShieldPool

			requestShieldNum = usedShieldNum;

			int timesNeededToFillUp = policyPoolRefillService.getFillUpTimes(POOL_SHIELD_SIZE, countShieldPool);

			int lastFillNum = 0;
			if (timesNeededToFillUp > 1) {
				lastFillNum = policyPoolRefillService.getLastFillNumber(POOL_SHIELD_SIZE, countShieldPool);
			} else {
				lastFillNum = usedShieldNum;
			}

			Log.debug("***Shield pool should refill times=" + timesNeededToFillUp + ", lastFillNum=" + lastFillNum);

			policyPoolRefillService.getPolicyNumber(uuid, timesNeededToFillUp, lastFillNum, true);

		}

		// NON Shield pool
		POOL_NON_SHIELD nonShieldPoolDao = new POOL_NON_SHIELD();
		int countNonShieldPool = POOL_NON_SHIELD_SIZE;

		try {
			countNonShieldPool = nonShieldPoolDao.selectRowCount();

		} catch (Exception ex) {
			Log.error(ex);
		}

		int usedNonShieldNum = POOL_NON_SHIELD_SIZE - countNonShieldPool;
		Log.debug("*** countNonShieldPool=" + countNonShieldPool + ", POOL_NON_SHIELD_SIZE=" + POOL_NON_SHIELD_SIZE
				+ ", usedNonShieldNum=" + usedNonShieldNum);

		if (usedNonShieldNum > 0) { // ShieldPool

			requestNonShieldNum = usedNonShieldNum;

			int timesNeededToFillUp = policyPoolRefillService.getFillUpTimes(POOL_NON_SHIELD_SIZE, countNonShieldPool);

			int lastFillNum = 0;
			if (timesNeededToFillUp > 1) {
				lastFillNum = policyPoolRefillService.getLastFillNumber(POOL_NON_SHIELD_SIZE, countNonShieldPool);
			} else {
				lastFillNum = usedNonShieldNum;
			}

			Log.debug("***NON Shield pool should refill times=" + timesNeededToFillUp + ", lastFillNum=" + lastFillNum);

			policyPoolRefillService.getPolicyNumber(uuid, timesNeededToFillUp, lastFillNum, false);

		}

		try {
			POOL_REFILL_TRX trxPoolRefillDao = new POOL_REFILL_TRX();
			boolean result = trxPoolRefillDao.update(uuid, countShieldPool, countNonShieldPool, POOL_SHIELD_SIZE,
					POOL_NON_SHIELD_SIZE, requestShieldNum, requestNonShieldNum);
			Log.debug("***trxPoolRefillDao update=" + result);
		} catch (Exception ex) {
			Log.error(ex);
		}

		Log.debug("*********************        PolicyPoolRefillJob   END     ***********************");

	}
}