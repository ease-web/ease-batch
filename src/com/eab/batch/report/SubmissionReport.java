package com.eab.batch.report;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.RPT_DIRECT_SUBMIT_TRX;
import com.eab.dao.RPT_SINGPOST_SUBMIT_TRX;
import com.eab.dao.ReportTransactionDao;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SubmissionReport {

	private final static SubmissionReportService submissionReportService = new SubmissionReportService();
	private final static SubmissionReportProvider reportProvider = new SubmissionReportProvider();
	
	private final static DateTimeFormatter DATE_FORMAT_AXA_EXCEL_NAME = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_AXA_EXCEL_NAME);
	
	private final static DateTimeFormatter DATE_FORMAT_SQL = DateTimeFormatter.ofPattern(Constant.DATETIME_PATTERN_AS_SQL_DATE);
	
	private final static String FILE_NAME_PREFIX = "FC_SubmissionReport_";

	private final static String DIRECT_CHANNEL = "DIRECT";
	
	public static boolean generateReport(String channel) {
		Log.info("Start SubmissionReportJob");

		Log.debug("************************ Start SubmissionReportJob ****************************** ");
		
		boolean completed = false;
		ReportTransactionDao reportDao = null;
		if (channel == DIRECT_CHANNEL) {
			reportDao = new RPT_DIRECT_SUBMIT_TRX();
		} else {
			reportDao = new RPT_SINGPOST_SUBMIT_TRX();
		}

		String uuid = UUID.randomUUID().toString();
		try {
			boolean createResult = reportDao.create(uuid);
			Log.debug("*** reportDao.create=" + createResult);
		} catch (Exception ex) {
			Log.error(ex);
		}

		try {
			JSONObject lastCheckRecord = reportDao.selectLastCheckPointTime();
			
			java.sql.Date fromTime = Function.convertJavaDateToSqlDate(new Date(0)); //Date time from beginning 
			
			if(lastCheckRecord != null) {
				Log.debug("*** lastCheckRecord=" + lastCheckRecord);
				if(lastCheckRecord.has("MAX_DT")) {
					String lastCheckTimeString = lastCheckRecord.get("MAX_DT").toString();
					
					ZonedDateTime zoneDate = ZonedDateTime.parse(lastCheckTimeString,
							DATE_FORMAT_SQL.withZone(ZoneId.of("UTC")));
					java.util.Date utilDate = java.util.Date.from ( zoneDate.toInstant() ) ;
					fromTime = Function.convertJavaDateToSqlDate(utilDate);
					
					Log.debug("*** fromTime=" + fromTime.toString());
				}
			}
			
			ZonedDateTime currentDate = Function.getDateUTCNow();
			java.sql.Date toTime = Function.covertZonedDateTimeToSqlDate(currentDate);

			JSONArray submittedCasesArray = submissionReportService.submittedCasesToRLS(fromTime, toTime);
			Log.debug("**** submittedCasesArray=" + submittedCasesArray);
			
			JSONArray pendingCasesArray = null;
			JSONObject pendingCasesView = Constant.CBUTIL.getRecordsByViewAfterIndex("submissionRptPendingDetails", "[\"01\",\""+channel+"\","+fromTime.getTime()+"]", "[\"01\",\""+channel+"\","+toTime.getTime()+"]");
			if (pendingCasesView != null && pendingCasesView.has("rows")) {
				pendingCasesArray = pendingCasesView.getJSONArray("rows");
			}
			Log.debug("**** pendingCasesArray=" + pendingCasesArray);

			if (submittedCasesArray != null) {	
				boolean updateCountFromLocalDB = reportDao.updateCountFromLocalDB(uuid, fromTime, toTime, submittedCasesArray.length()) ;
				Log.debug("*** updateCountFromLocalDB=" + updateCountFromLocalDB);
			}
			
			if (pendingCasesArray != null) {
				boolean updateCountFromCouchbase = reportDao.updateCountFromCouchbase(uuid, pendingCasesArray.length());
				Log.debug("*** updateCountFromCouchbase=" + updateCountFromCouchbase);
			}
			
			JSONArray resubmittedCasesStatusArray = submissionReportService.resubmittedCasesOriginalStatus(fromTime, toTime);
			
			String excelFileData = null;
			String reportPassword = submissionReportService.generateReportPassword();
			
			ArrayList<String> eAppIdArrayList = submissionReportService.extractApplicationIdArray(pendingCasesArray, submittedCasesArray);
				
			if (eAppIdArrayList.size() > 0) {
				Log.debug("**** eAppIdArrayList.size()=" + eAppIdArrayList.size());
				//filter cases on Singpost only
				JSONArray eAppsResultJsonArray = submissionReportService.getEAppsOnCouchBase(channel, eAppIdArrayList);
				Log.debug("**** eAppsResultJsonArray=" + eAppsResultJsonArray);

				if (eAppsResultJsonArray != null && eAppsResultJsonArray.length() > 0) {

					ArrayList<String> agentCodeArray = submissionReportService.extractAgentCodeArray(eAppsResultJsonArray);
					
					int requestAgentCount = agentCodeArray.size();
					int responseAgentCount = 0;
					
					JSONArray agentResultJsonArray = submissionReportService.getAgentsOnCouchBase(agentCodeArray);

					if (agentCodeArray.size() > 0) {
						Log.debug("**** agentCodeArray.size()=" + agentCodeArray.size());

						if (agentResultJsonArray != null) {
							responseAgentCount = agentResultJsonArray.length();
//										Log.debug("**** agentResultJsonArray=" + agentResultJsonArray);

							boolean updateAgentCountFromCouchbase = reportDao.updateAgentCountFromCouchbase(uuid, requestAgentCount, responseAgentCount);
//										Log.debug("*** updateAgentCountFromCouchbase=" + updateAgentCountFromCouchbase);

							HashMap<String, JSONObject> eAppsByAppIdMap = submissionReportService
									.convertAppsToHashMapByAppId(eAppsResultJsonArray);
							HashMap<String, JSONObject> mergedSubmissionDateMap = submissionReportService
									.mergeSubmissionDateForEApps(eAppsByAppIdMap, submittedCasesArray, resubmittedCasesStatusArray);
//										Log.debug("**** mergedSubmissionDateMap=" + mergedSubmissionDateMap);
							HashMap<String, JSONObject> mergedApprovalStatusMap = submissionReportService
									.mergePendingApprovalStatusForEApps(eAppsByAppIdMap, pendingCasesArray);
//										Log.debug("**** mergedSubmissionDateMap=" + mergedSubmissionDateMap);
							HashMap<String, JSONObject> mergedManagerNameMap = submissionReportService
									.mergeManagerNameForEApps(eAppsByAppIdMap, agentResultJsonArray);
//										Log.debug("**** mergedManagerNameMap=" + mergedManagerNameMap);

							JsonArray convertedJsonArray = submissionReportService
									.convertEAppsMapToJsonArray(mergedManagerNameMap, eAppIdArrayList);
							Log.debug("**** convertedJsonArray=" + convertedJsonArray);

							String[][] excelRowData = submissionReportService
									.parseToExcelDataArray(convertedJsonArray);

							if (excelRowData != null) {
								excelFileData = reportProvider.createExcelReportInBase64Format(excelRowData, reportPassword);
							} else {
								Log.error("ERROR - excelRowData for excel");
							}
						}else {
							Log.error("ERROR - agentResultJsonArray is null");
						}
					}else {
						Log.error("ERROR - agentCode array not extracted");
					}
				}else {
					Log.error("ERROR - eAppsResultJsonArray is empty");
					//no cases
					excelFileData = reportProvider.createEmptyReport(reportPassword);
				}
			} else {
				Log.error("ERROR - eAppId not extracted");
				//No submission 
				excelFileData = reportProvider.createEmptyReport(reportPassword);
			}
			
			if(excelFileData != null) {
				ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID));
				String convertedDateString = DATE_FORMAT_AXA_EXCEL_NAME.format(dateTime);

				String fileName = FILE_NAME_PREFIX + convertedDateString + ".xls";
				
				JsonObject reportJsonBody = submissionReportService.createReportJsonBody(channel, fileName, excelFileData);
				
				boolean isSendReportCompleted = false;
				if(reportJsonBody != null) {
					HttpPost postRequest = submissionReportService.createEmailPostRequest(reportJsonBody);
					Response output = HttpUtil.send(postRequest);
					
					isSendReportCompleted = output.getStatus() == 204? true:false;
					Log.debug("**** Send Report=" + isSendReportCompleted);
					
					java.sql.Date emailTime = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
					boolean updateFromSendingEmail = reportDao.updateFromSendingEmail(uuid, fileName, emailTime, isSendReportCompleted);
					Log.debug("**** updateFromSendingEmail=" + updateFromSendingEmail);
				}else {
					Log.error("ERROR - submissionReportService.createReportJsonBody jsonBody is null");
				}
				
				boolean isSendPasswordCompleted = false;
				JsonObject passwordJsonBody = submissionReportService.createPasswordJsonBody(channel, reportPassword);
				
				if(reportJsonBody != null) {
					HttpPost postRequest = submissionReportService.createEmailPostRequest(passwordJsonBody);
					Response output = HttpUtil.send(postRequest);
					
					isSendPasswordCompleted = output.getStatus() == 204? true:false;
					
					Log.debug("**** Send Report Password isCompleted=" + isSendPasswordCompleted);
					
					Log.debug("**** email status =" + output.getStatus());
					
					if(!isSendPasswordCompleted) {
						Log.debug("**** Fail to send report" + output.getEntity().toString());
					}

				}else {
					Log.error("ERROR - submissionReportService.createPasswordJsonBody jsonBody is null");
				}
				
				completed = isSendReportCompleted && isSendPasswordCompleted;
			}
		} catch (Exception ex) {
			Log.error(ex);
		}
		Log.debug("************************ End SubmissionReportJob ****************************** ");
		return completed;
	}
}
