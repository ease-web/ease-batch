package com.eab.batch.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.batch.report.PasswordGenerator.PasswordCharacterSet;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.BATCH_EMAIL_TEMPLATE;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.eab.dao.SYSTEM_PARAM;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SubmissionReportService {
	
	private final int MIN_PW_LENGTH = 8;
	private final int MAX_PW_LENGTH = 12;
	
	private static final char[] ALPHA_UPPER_CHARACTERS = { 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private static final char[] ALPHA_LOWER_CHARACTERS = { 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
    private static final char[] NUMERIC_CHARACTERS = { '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9' };
    private static final char[] SPECIAL_CHARACTERS = { '~', '!', '@', '#',
            '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '|', ';', ',', '?' };
	
	private final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private final String SERVICE_PATH = "/email/sendEmail";
	
//	private final String SINGPOST_APP_VIEW_NAME = "singpostApps";
	private final String SUBMISSION_APP_VIEW_NAME = "submissionRptApps";
	private final String AGENT_VIEW_NAME = "agentDetails";
	
	private final String DIRECT_CHANNEL = "DIRECT";
	
	private String getApprovalStatusString(String approvalStatus) {
		switch (approvalStatus) {
			case "A":
				return "Approved";
			case "R":
				return "Rejected";
			case "E":
				return "Expired";
			case "SUBMITTED":
				return "Pending Supervisor Approval";
			case "PDoc":
				return "Pending Document";
			case "PDis":
				return "Pending Discussion";
			default:
				return "";
		}
	}

	public JSONArray submittedCasesToRLS(java.sql.Date fromTime, java.sql.Date toTime) {
		Log.info("Goto SubmissionReportService.submittedCasesToRLS");
		
		BATCH_SUBMISSION_REQUEST submissionDao = new BATCH_SUBMISSION_REQUEST();
		
		
		JSONArray resultArray = null;
		
		try {
			resultArray = submissionDao.selectSubmittedCasesToRLS(fromTime,toTime);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return resultArray;
	}
	
	public JSONArray resubmittedCasesOriginalStatus(java.sql.Date fromTime, java.sql.Date toTime) {
		Log.info("Goto SubmissionReportService.resubmittedCasesOriginalStatus");
		
		BATCH_SUBMISSION_REQUEST submissionDao = new BATCH_SUBMISSION_REQUEST();
		
		JSONArray resultArray = null;
		try {
			resultArray = submissionDao.selectReSubmittedCasesToRLSOriginalStatus(fromTime,toTime);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return resultArray;
	}

	public JSONArray getEAppsOnCouchBase(String channel, ArrayList<String> eAppIdArray) {
		Log.info("Goto SubmissionReportService.getEAppsOnCouchBase");
		
		String[][] array = new String[eAppIdArray.size()][];
		for (int i = 0; i < eAppIdArray.size(); i++) {
			ArrayList<String> keyList = new ArrayList<String>();
			String eAppNo = eAppIdArray.get(i);
			keyList.add("01");
			keyList.add(channel);
			keyList.add(eAppNo);
			
		    array[i] = keyList.toArray(new String[keyList.size()]);
		}
	
		return Constant.CBUTIL.getDocsByView(SUBMISSION_APP_VIEW_NAME, array);
	}
	
	
	public JSONArray getAgentsOnCouchBase(ArrayList<String> agentCodeArray) {
		Log.info("Goto SubmissionReportService.getAgentsOnCouchBase");
		
		String[][] array = new String[agentCodeArray.size()][];
		for (int i = 0; i < agentCodeArray.size(); i++) {
			ArrayList<String> keyList = new ArrayList<String>();
			String agentCode = agentCodeArray.get(i);
			keyList.add("01");
			keyList.add("agentCode");
			keyList.add(agentCode);
			
		    array[i] = keyList.toArray(new String[keyList.size()]);
		}
		
		return Constant.CBUTIL.getDocsByView(AGENT_VIEW_NAME, array);
	}
	
	public ArrayList<String> extractApplicationIdArray(JSONArray pendingCasesArray, JSONArray submittedCasesArray){
		Log.info("Goto SubmissionReportService.extractApplicationIdArray");
		ArrayList<String> arrayList = new ArrayList<String>();
		
		if (pendingCasesArray != null) {
			for(Object item : pendingCasesArray) {
				JSONObject jsonObject = (JSONObject) item;
				if(jsonObject.has("value")) {
					JSONObject value = jsonObject.getJSONObject("value");
					String eAppNo = value.getString("applicationId");
					arrayList.add(eAppNo);
				}
			}
		}

		if (submittedCasesArray != null) {
			for(Object item : submittedCasesArray) {
				JSONObject jsonObject = (JSONObject) item;
				String EAPP_NO = jsonObject.getString("EAPP_NO");
				
				if (!arrayList.contains(EAPP_NO)) {
					arrayList.add(EAPP_NO);
				}
			}
		}

		return arrayList;
	}
	
	public ArrayList<String> extractAgentCodeArray(JSONArray applicationIdJSONArray){
		Log.info("Goto SubmissionReportService.extractAgentCodeArray");
		
		ArrayList<String> arrayList = new ArrayList<String>();
		
		for(Object item : applicationIdJSONArray) {
			JSONObject jsonObject = (JSONObject) item;
			if(jsonObject.has("value")) {
				JSONObject value = jsonObject.getJSONObject("value");
				
				if(value.has("aCode")) {
					String agentCode = value.getString("aCode");
					arrayList.add(agentCode);
				}
			}
		}
		
		return arrayList;
	}
	
	public HashMap<String, JSONObject> convertAppsToHashMapByAppId(JSONArray eAppsJsonArray) {
		Log.info("Goto SubmissionReportService.convertAppsToHashMapByAppId");
		
		HashMap<String, JSONObject> eAppsByAppIdMap = new HashMap<String, JSONObject>();
		
		for(Object item : eAppsJsonArray) {
			JSONObject jsonObject = (JSONObject) item;
			
			if(jsonObject.has("value") && jsonObject.has("id")) {
				JSONObject value = (JSONObject) jsonObject.get("value");
				
				String id = jsonObject.getString("id");
				eAppsByAppIdMap.put(id, value);
			}
		}
		
		return eAppsByAppIdMap;
	}
	
	public HashMap<String, JSONObject> mergeSubmissionDateForEApps(HashMap<String, JSONObject> eAppsByAppIdMap, JSONArray submittedCasesToRLSArray, JSONArray resubmittedCasesStatusArray) {
		Log.info("Goto SubmissionReportService.mergeSubmissionDateForEApps");
		HashMap<String, String> appIdStatusMap = new HashMap<String, String>(); 
		
		if (resubmittedCasesStatusArray != null) {
			for(Object item : resubmittedCasesStatusArray) {
				JSONObject jsonObject = (JSONObject) item;
				String EAPP_NO = jsonObject.getString("EAPP_NO");
				String TRIGGER_BY_PROCESS = jsonObject.getString("TRIGGER_BY_PROCESS");
				appIdStatusMap.put(EAPP_NO, TRIGGER_BY_PROCESS);
			}
		}
		
		if (submittedCasesToRLSArray != null) {
			for(Object item : submittedCasesToRLSArray) {
				JSONObject jsonObject = (JSONObject) item;
				String EAPP_NO = jsonObject.getString("EAPP_NO");
				Object RLS_SUBMISSION_DATE = jsonObject.get("RLS_SUBMISSION_DATE");
				String TRIGGER_BY_PROCESS = jsonObject.getString("TRIGGER_BY_PROCESS");
				
				JSONObject eAppObject = eAppsByAppIdMap.get(EAPP_NO);
				
				if(eAppObject !=null) {
					eAppObject.put("date", RLS_SUBMISSION_DATE);
					if (TRIGGER_BY_PROCESS.equals("API_RESUBMISSION") && appIdStatusMap.containsKey(EAPP_NO)) {
						eAppObject.put("approvalStatus", appIdStatusMap.get(EAPP_NO));
					} else if (TRIGGER_BY_PROCESS.equals("Invalidated Paid")) {
						eAppObject.put("approvalStatus", TRIGGER_BY_PROCESS.replaceAll("Paid", "(Paid)"));
					} else {
						eAppObject.put("approvalStatus", TRIGGER_BY_PROCESS);
					}
					eAppsByAppIdMap.put(EAPP_NO, eAppObject);
				}
			}
		}

		return eAppsByAppIdMap;
	}
	
	public HashMap<String, JSONObject> mergePendingApprovalStatusForEApps(HashMap<String, JSONObject> eAppsByAppIdMap, JSONArray pendingCasesArray) {
		Log.info("Goto SubmissionReportService.mergePendingApprovalStatusForEApps");
		
		if (pendingCasesArray != null) {
			for(Object item : pendingCasesArray) {
				JSONObject jsonObject = (JSONObject) item;
				if(jsonObject.has("value")) {
					JSONObject value = jsonObject.getJSONObject("value");
					String eAppNo = value.getString("applicationId");
					
					JSONObject eAppObject = eAppsByAppIdMap.get(eAppNo);
					
					if(eAppObject !=null) {
						eAppObject.put("approvalStatus", getApprovalStatusString(value.getString("approvalStatus")));
						eAppsByAppIdMap.put(eAppNo, eAppObject);
					}
				}
			}
		}

		return eAppsByAppIdMap;
	}

	
	public HashMap<String, JSONObject> mergeManagerNameForEApps(HashMap<String, JSONObject> eAppsByAppIdMap, JSONArray agentResultJsonArray) {
		Log.info("Goto SubmissionReportService.mergeManagerNameForEApps");
		
		HashMap<String, String> agentManagerNameByAgentCodeMap = new HashMap<String, String>();
		
		for(Object item : agentResultJsonArray) { //create a map that divided agent Manager Name By AgentCode
			JSONObject jsonObject = (JSONObject) item;
			
			if(jsonObject.has("value") && jsonObject.has("id")) {
				JSONObject value = (JSONObject) jsonObject.get("value");
				
				String agentCode = value.getString("agentCode");
				String manager = value.getString("manager");
				agentManagerNameByAgentCodeMap.put(agentCode, manager);
			}
		}
		
		for (String key : eAppsByAppIdMap.keySet()) {
			JSONObject eAppObject = eAppsByAppIdMap.get(key);
			
			if(eAppObject.has("aCode")) {
				String agentCode = eAppObject.getString("aCode");
				
				String manager = agentManagerNameByAgentCodeMap.get(agentCode);
				
				if(manager !=null)
				{
					eAppObject.put("manager", manager);
					eAppsByAppIdMap.put(key, eAppObject);
				}
			}
		}
		
		return eAppsByAppIdMap;
	}
	
	public JsonArray convertEAppsMapToJsonArray(HashMap<String, JSONObject> eAppsByAppIdMap, ArrayList<String> eAppIdArrayList) {
		Log.info("Goto SubmissionReportService.convertEAppsMapToJsonArray");
		
		JsonArray jsonArray = new JsonArray();
		
		for(String key: eAppIdArrayList) { // records order by eAppIdArrayList
			JSONObject eAppObject = eAppsByAppIdMap.get(key);
			
			if(eAppObject!=null) {
				String eAppObjString = eAppObject.toString();
				JsonObject jsonObject = Function.validateJsonFormatForString(eAppObjString);
				jsonArray.add(jsonObject);
			}
		}
		
		return jsonArray;
	}
	
	public String[][] parseToExcelDataArray(JsonArray eAppsJsonArray) {
		Log.info("Goto SubmissionReportService.parseToExcelDataArray");

		ArrayList<String[]> rowArrayList = new ArrayList<String[]>();
		int sequenceNum = 1;
		for (JsonElement item : eAppsJsonArray) {

//			Log.debug("***************  sequenceNum =" + sequenceNum + "     *************");

			JsonObject eAppJsonObject = item.getAsJsonObject();
//			Log.debug("*** eAppJsonObject =" + eAppJsonObject);

			String dateString = eAppJsonObject.has("date") ? eAppJsonObject.get("date").getAsString() : null;
			String statusString = eAppJsonObject.has("approvalStatus") ? eAppJsonObject.get("approvalStatus").getAsString() : null;
			String policyNumberString = eAppJsonObject.has("policyNumber")
					? eAppJsonObject.get("policyNumber").getAsString()
					: null;
					
			Log.debug("***************  policyNumberString =" + policyNumberString + "     *************");
			String proposerId = eAppJsonObject.has("pId") ? eAppJsonObject.get("pId").getAsString() : null;
			String proposerName = eAppJsonObject.has("pName") ? eAppJsonObject.get("pName").getAsString()
					: null;
			String agentName = eAppJsonObject.has("aName") ? eAppJsonObject.get("aName").getAsString() : null;
			String managerName = eAppJsonObject.has("manager") ? eAppJsonObject.get("manager").getAsString()
					: null;
			String bankRefId = eAppJsonObject.has("bankRefId") ? eAppJsonObject.get("bankRefId").getAsString()
					: null;
			
			JsonObject planDetails = eAppJsonObject.has("planDetails") ? eAppJsonObject.get("planDetails").getAsJsonObject()
					: null;
			
			boolean isShield = false;

			// Plans
			if (eAppJsonObject.has("quotation") && eAppJsonObject.has("productCode")) {
				
				JsonObject quotationObject = eAppJsonObject.get("quotation").getAsJsonObject();
				
				double topupRegular = 0;
				double topupLumpsum = 0;
				
				JsonObject policyOptionsObject = quotationObject.has("policyOptions")?quotationObject.get("policyOptions").getAsJsonObject():null;
				
				if(policyOptionsObject !=null) {
					topupRegular = policyOptionsObject.has("rspAmount") && !policyOptionsObject.get("rspAmount").isJsonNull()? policyOptionsObject.get("rspAmount").getAsDouble(): 0;
					
					if(topupRegular !=0)
					{
						topupRegular*=12;//flexi protector only has monthly
					}
					
					topupLumpsum = policyOptionsObject.has("topUpAmt") && !policyOptionsObject.get("topUpAmt").isJsonNull()? policyOptionsObject.get("topUpAmt").getAsDouble(): 0;
				}
				
				if(quotationObject.has("quotType")) {
					String quotType =  quotationObject.get("quotType").getAsString();
					
					if(quotType !=null && quotType.equals("SHIELD")) {
						isShield = true;
					}
				}
				
				String paymentMode = quotationObject.has("paymentMode") ? quotationObject.get("paymentMode").getAsString()
						: null;
				
				String productCode = eAppJsonObject.get("productCode").getAsString();
				
				double totalApe = 0;
				String planName = null;
				
				JsonArray plansJsonArray = null;
				
				if(quotationObject.has("plans") && !isShield) {
					plansJsonArray = quotationObject.get("plans").getAsJsonArray();
					
					if(plansJsonArray != null) {
						for (int i = 0; i < plansJsonArray.size(); i++) {
							JsonElement planItem = plansJsonArray.get(i);
							JsonObject planJsonObject = planItem.getAsJsonObject();

							double singlePremium = 0;
							double annualizedRegularPremium = 0;
							double annualizedRegularHalfYearPremium = 0;
							double annualizedRegularQuarterPremium = 0;
							double annualizedRegularMonthPremium = 0;
							
							double halfYearTax = 0;
							double monthTax = 0;
							double quarterTax = 0;
							double yearTax = 0;
							
							if (planJsonObject.has("premium")) {
								singlePremium = planJsonObject.get("premium").getAsDouble();
							}

							if (planJsonObject.has("yearPrem")) {
								annualizedRegularPremium = planJsonObject.get("yearPrem").getAsDouble();
							}
							
							if (planJsonObject.has("halfYearPrem")) {
								annualizedRegularHalfYearPremium = planJsonObject.get("halfYearPrem").getAsDouble();
							}
							
							if (planJsonObject.has("quarterPrem")) {
								annualizedRegularQuarterPremium = planJsonObject.get("quarterPrem").getAsDouble();
							}
							
							if (planJsonObject.has("monthPrem")) {
								annualizedRegularMonthPremium = planJsonObject.get("monthPrem").getAsDouble();
							}
							
							if (planJsonObject.has("tax")) {
								JsonObject taxObject = planJsonObject.get("tax").getAsJsonObject();
								if(taxObject.has("halfYearTax")) {
									halfYearTax = taxObject.get("halfYearTax").getAsDouble();
								}
								if(taxObject.has("monthTax")) {
									monthTax = taxObject.get("monthTax").getAsDouble();
								}
								if(taxObject.has("quarterTax")) {
									quarterTax = taxObject.get("quarterTax").getAsDouble();
								}
								if(taxObject.has("yearTax")) {
									yearTax = taxObject.get("yearTax").getAsDouble();
								}
							}
							
							Log.debug("***policyNumber="+policyNumberString+", annualizedRegularPremium="+annualizedRegularPremium+", singlePremium="+singlePremium);

							
							double ape = calculateAPE(paymentMode, productCode, singlePremium, annualizedRegularPremium, annualizedRegularHalfYearPremium + halfYearTax, annualizedRegularQuarterPremium + quarterTax, annualizedRegularMonthPremium + monthTax, yearTax,topupRegular,topupLumpsum);
							
							totalApe += ape;

							if (planJsonObject.has("covName") && i==0) { //Use basic plan name only
								JsonObject covName = planJsonObject.get("covName").getAsJsonObject();
								Log.debug("*** covName =" + covName);
								if (covName.has("en")) {
									planName = covName.get("en").getAsString();
									Log.debug("*** planName =" + planName);
								}
							}
						}
						
						double apeRoundOff = Math.round(totalApe * 100.0) / 100.0; // round up to 2 decimal places
						String apeRoundOffString = String.valueOf(apeRoundOff);
						
						String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, statusString, policyNumberString, proposerId,
								proposerName, agentName, managerName, bankRefId, planName, apeRoundOffString);
						rowArrayList.add(rowArray);
						
						sequenceNum++;
					}
					
				}else if(planDetails !=null & isShield){
					
					if(planDetails.has("planList")) {
						plansJsonArray = planDetails.get("planList").getAsJsonArray();
					}
					
					if(plansJsonArray != null) {
						for (int i = 0; i < plansJsonArray.size(); i++) {
							JsonElement planItem = plansJsonArray.get(i);
							JsonObject planJsonObject = planItem.getAsJsonObject();

							double singlePremium = 0;
							double annualizedRegularPremium = 0;
							double yearTax = 0;

							if (planJsonObject.has("premium")) {
								singlePremium = planJsonObject.get("premium").getAsDouble();
							}

							if (planJsonObject.has("yearPrem")) {
								annualizedRegularPremium = planJsonObject.get("yearPrem").getAsDouble();
							}
							
							if(isShield && planJsonObject.has("planCode")) {
								String planCode = planJsonObject.get("planCode").getAsString();
								
								String[] shieldBasicPlanCodes = new String[] { "ASIMSA","ASIMSB", "ASIMSC" };
								boolean isShieldBasicPlan = Arrays.asList(shieldBasicPlanCodes).contains(planCode);
								
								Log.debug("*** isShieldBasicPlan="+isShieldBasicPlan);
								
								if(isShieldBasicPlan) {
									annualizedRegularPremium = planDetails.has("axaShield")? planDetails.get("axaShield").getAsDouble():0;
								}
								
							}
							
							// Paul confirms tax value is already included in premium
//							if (planJsonObject.has("tax")) {
//								JsonElement taxElement = planJsonObject.get("tax");
//								if(taxElement.isJsonObject()) {
//									JsonObject taxObject = taxElement.getAsJsonObject();
//									if(taxObject.has("yearTax")) {
//										yearTax = taxObject.get("yearTax").getAsDouble();
//									}
//								}
//							}
							
							Log.debug("***Shield policyNumber="+policyNumberString+", annualizedRegularPremium="+annualizedRegularPremium+", singlePremium="+singlePremium);
							
							double ape = calculateShieldAPE(annualizedRegularPremium, yearTax);

							if (planJsonObject.has("covName")) {
								JsonObject covName = planJsonObject.get("covName").getAsJsonObject();
								Log.debug("*** covName =" + covName);
								if (covName.has("en")) {
									planName = covName.get("en").getAsString();
									Log.debug("*** planName =" + planName);
								}
							}
							
							double apeRoundOff = Math.round(ape * 100.0) / 100.0; // round up to 2 decimal places
							String apeRoundOffString = String.valueOf(apeRoundOff);
							
							String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, statusString, policyNumberString, proposerId,
									proposerName, agentName, managerName, bankRefId, planName, apeRoundOffString);
							rowArrayList.add(rowArray);
							
							sequenceNum++;
						}
					}
				}


			} else {
				Log.debug("***  no plans");
				String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, statusString, policyNumberString, proposerId,
						proposerName, agentName, managerName, bankRefId, null, null);
				rowArrayList.add(rowArray);
				sequenceNum++;

			}

		}

		return convertArrayListTo2DStringArray(rowArrayList);
	}

	private String[][] convertArrayListTo2DStringArray(ArrayList<String[]> rowArrayList) {
		Log.info("Goto SubmissionReportService.convertArrayListTo2DStringArray");

		String[][] array = new String[rowArrayList.size()][];
		for (int i = 0; i < rowArrayList.size(); i++) {
			String[] stringArray = rowArrayList.get(i);
			array[i] = stringArray;
		}

		return array;
	}
	
	private double calculateShieldAPE(double annualizedRegularPremium, double yearTax) {
		Log.info("Goto SubmissionReportService.calculateShieldAPE");
		
		//Annual premium equivalent (APE)
		double ape = annualizedRegularPremium + yearTax;
		return ape;
	}

	private double calculateAPE(String paymentMode, String productCode, double singlePremium,
			double annualizedRegularPremium, double annualizedRegularHalfYearPremium, double annualizedRegularQuarterPremium, double annualizedRegularMonthPremium, 
			double yearTax, double topupRegular, double topupLumpsum) {
		Log.info("Goto SubmissionReportService.calculateAPE");
		Log.debug("*** paymentMode="+paymentMode+", productCode="+productCode+", singlePremium="+singlePremium+",annualizedRegularPremium="+annualizedRegularPremium+", yearTax="+yearTax);

		// 100% Annualized Regular Premium + 10% Top-up Regular + 10% Single Premium +
		// 10% Top-up Lump Sum

		double singlePremiumInTenPercent = 0;
		double topupRegularInTenPercent = topupRegular!=0?topupRegular*0.1:0; // + 10% Top-up Regular
		double topupLumpSumInTenPercent = topupLumpsum!=0?topupLumpsum*0.1:0; // + 10% Top-up lump sum
		double regularPremium = 0;
		if (paymentMode !=null) { 

			if(paymentMode.equals("L")) {
				// Inspire Duo plan only has Single Premium. Thus, we only need "premium"
				annualizedRegularPremium = 0; // "yearPrem" is a value by regular premium. Hence, it's not applicable for Single Premium case 
				singlePremiumInTenPercent = singlePremium * 0.1;
			}else {
				// For other product, we need "yearPrem" for regular premium
				singlePremiumInTenPercent = 0; // should ignore Single Premium value
				switch(paymentMode){
					case "A":
						regularPremium = annualizedRegularPremium;
						break;
					case "S":
						regularPremium = annualizedRegularHalfYearPremium * 2;
						break;
					case "Q":
						regularPremium = annualizedRegularQuarterPremium * 4;
						break;
					case "M":					
						regularPremium = annualizedRegularMonthPremium * 12;
						break;
					default:
						regularPremium = annualizedRegularPremium;
						break;
				}
			}
		} else {
			Log.error("ERROR - Missing paymentMode value");
		}
		
		//Annual premium equivalent (APE)
//		double ape = regularPremium + topupRegularInTenPercent + singlePremiumInTenPercent
//				+ topupLumpSumInTenPercent + yearTax;
		
		
		// remove year tax
		double ape = regularPremium + topupRegularInTenPercent + singlePremiumInTenPercent
				+ topupLumpSumInTenPercent;
		
		return ape;
	}

	private String[] createRowArray(String sequenceNumString, String dateString, String statusString, String policyNumberString,
			String proposerId, String proposerName, String agentName, String managerName, String bankRefId,
			String planName, String annualizedPremium) {
		Log.info("Goto SubmissionReportService.createRowArray");
		
		ArrayList<String> rowList = new ArrayList<String>();

		rowList.add(sequenceNumString);
		rowList.add(dateString);
		rowList.add(statusString);
		rowList.add(policyNumberString);
		rowList.add(proposerId);
		rowList.add(proposerName);
		rowList.add(agentName);
		rowList.add(managerName);
		rowList.add(bankRefId);
		rowList.add(planName);
		rowList.add(annualizedPremium);

		return rowList.toArray(new String[rowList.size()]);
	}

	public JsonObject createReportJsonBody(String channel, String fileName, String fileDataBase64Format) throws Exception {
		Log.info("Goto SubmissionReportService.createReportJsonBody");
		
		JsonObject jsonBody = null;
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = channel == DIRECT_CHANNEL ? eSysParam.selectSysValue("REPORT_RLS_DIRECT_TO") : eSysParam.selectSysValue("REPORT_RLS_SINGPOST_TO");
			String fromEmail = channel == DIRECT_CHANNEL ? eSysParam.selectSysValue("REPORT_RLS_DIRECT_FROM") : eSysParam.selectSysValue("REPORT_RLS_SINGPOST_FROM");
			
			Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			
			BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			
			JSONObject templateJsonObject = channel == DIRECT_CHANNEL ? emailTemplateJson.selectEmailTemplate("REPORT_RLS_DIRECT") : emailTemplateJson.selectEmailTemplate("REPORT_RLS_SINGPOST");
			
			String title = templateJsonObject.has("MAIL_SUBJ")? templateJsonObject.getString("MAIL_SUBJ"):null;
			String content = templateJsonObject.has("MAIL_CONTENT")? templateJsonObject.getString("MAIL_CONTENT"):null;
			
			if(toEmail !=null && fromEmail !=null && title !=null && content !=null) {
				JsonObject fileJson = new JsonObject();
				fileJson.addProperty("fileName", fileName);
				fileJson.addProperty("data", fileDataBase64Format);

				JsonArray attachments = new JsonArray();
				attachments.add(fileJson);

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content",content);
				jsonBody.add("attachments", attachments);
			}else {
				Log.error("ERROR - Report - toEmail, fromEmail, title, content have null");
			}
			
		}catch(Exception ex){
			Log.error(ex);
		}

		return jsonBody;
	}

	public HttpPost createEmailPostRequest(JsonObject jsonBody) throws Exception {
		Log.info("Goto SubmissionReportService.createEmailPostRequest");

		String emailURL = DOMAIN + SERVICE_PATH;

		HttpPost request = new HttpPost(emailURL);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}
	
	public JsonObject createPasswordJsonBody(String channel, String password) throws Exception {
		Log.info("Goto SubmissionReportService.createPasswordJsonBody");
		
		JsonObject jsonBody = null;
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = channel == DIRECT_CHANNEL ? eSysParam.selectSysValue("REPORT_RLS_DIRECT_TO") : eSysParam.selectSysValue("REPORT_RLS_SINGPOST_TO");
			String fromEmail = channel == DIRECT_CHANNEL ? eSysParam.selectSysValue("REPORT_RLS_DIRECT_FROM") : eSysParam.selectSysValue("REPORT_RLS_SINGPOST_FROM");
			
			Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			
			BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			
			JSONObject templateJsonObject = channel == DIRECT_CHANNEL ? emailTemplateJson.selectEmailTemplate("REPORT_RLS_DIRECT_PW") : emailTemplateJson.selectEmailTemplate("REPORT_RLS_SINGPOST_PW");
			
			String title = templateJsonObject.has("MAIL_SUBJ")? templateJsonObject.getString("MAIL_SUBJ"):null;
			String content = templateJsonObject.has("MAIL_CONTENT")? templateJsonObject.getString("MAIL_CONTENT"):null;
			
			password = StringEscapeUtils.escapeJson(password);
			
			content = content.replace("[password]", password);//password
			
			if(toEmail !=null && fromEmail !=null && title !=null && content !=null) {

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content",content);
			}else {
				Log.error("ERROR - Password - toEmail, fromEmail, title, content have null");
			}
			
		}catch(Exception ex){
			Log.error(ex);
		}


		return jsonBody;
	}
	
	public String generateReportPassword() {
		Set<PasswordCharacterSet> values = new HashSet<PasswordCharacterSet>(EnumSet.allOf(SummerCharacterSets.class));
        PasswordGenerator pwGenerator = new PasswordGenerator(values, MIN_PW_LENGTH, MAX_PW_LENGTH);
        
        char[] c = pwGenerator.generatePassword();
        
        return String.valueOf(c);
	}
	
	
	private enum SummerCharacterSets implements PasswordCharacterSet {
        ALPHA_UPPER(ALPHA_UPPER_CHARACTERS, 1),
        ALPHA_LOWER(ALPHA_LOWER_CHARACTERS, 1),
        NUMERIC(NUMERIC_CHARACTERS, 1),
        SPECIAL(SPECIAL_CHARACTERS, 1);

        private final char[] chars;
        private final int minUsage;

        private SummerCharacterSets(char[] chars, int minUsage) {
            this.chars = chars;
            this.minUsage = minUsage;
        }

        @Override
        public char[] getCharacters() {
            return chars;
        }

        @Override
        public int getMinCharacters() {
            return minUsage;
        }
    }

}
