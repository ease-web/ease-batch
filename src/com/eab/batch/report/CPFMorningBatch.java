package com.eab.batch.report;

import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.eab.batch.report.CPF;
import com.eab.common.Log;
import com.eab.dao.BATCH_LOG;

public class CPFMorningBatch implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		try {	
			stopWatch.start();			
			startBatch();
			stopWatch.stop();
			Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
			
		} catch (Exception e){
			Log.error(e);
		} finally {
			stopWatch = null;
		}
	}
	
	public void startBatch() throws Exception {
		BATCH_LOG batchLog = new BATCH_LOG();
		
		//Batch_log - S
		int batchNo = batchLog.selectSeq();
		int recordProcessed = 1; 
		String batchStatus = "P";	
		String jobName = "JOB_BATCH_SUBMIT_REPORT_CPF_AM";
		batchLog.selectSeq(); 
		batchLog.create(batchNo, jobName , batchStatus, new Date(), recordProcessed);
		
		String batchTime = "AM";
		boolean success = CPF.generateReport(batchTime);
		//Batch_log - E
		
		batchStatus = success ? "C" : "F";			 
		batchLog.update(batchNo, recordProcessed, batchStatus, "BATCH");
		
		if(!success){
			//CPF.sendAlertReport(batchTime);
		}
	}
	
}
