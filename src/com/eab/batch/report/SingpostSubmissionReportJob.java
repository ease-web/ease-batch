package com.eab.batch.report;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.RPT_SINGPOST_SUBMIT_TRX;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SingpostSubmissionReportJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		StopWatch stopWatch = new StopWatch();

		try {	
			stopWatch.start();			
			startBatch();
			stopWatch.stop();
			Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
			
		} catch (Exception e){
			Log.error(e);
		} finally {
			stopWatch = null;
		}
	}

	public static void startBatch() throws Exception {
		Log.info("Start SingpostSubmissionReportJob");

		BATCH_LOG batchLog = new BATCH_LOG();
		
		//Batch_log - S
		int batchNo = batchLog.selectSeq();
		int recordProcessed = 1; 
		String batchStatus = "P";	
		String jobName = "JOB_BATCH_SINGPOST_SUBMISSION_REPORT";
		batchLog.selectSeq(); 
		batchLog.create(batchNo, jobName , batchStatus, new Date(), recordProcessed);

		boolean success = SubmissionReport.generateReport("SINGPOST");
		//Batch_log - E
		
		batchStatus = success ? "C" : "F";			 
		batchLog.update(batchNo, recordProcessed, batchStatus, "BATCH");
		
		Log.debug("************************ End SingpostSubmissionReportJob ****************************** ");
	}
}
