package com.eab.batch.approvalnotification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendFAFirmCaseApprovalReminder implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
		
	public static void startBatch() {
		Log.info("Send FA Firm Case Approval Reminder Batch Job Start");
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_TO_SEND_PENDING_CASE_EMAIL_FOR_FAFIRM");
			int controlValue = (tempValue == null) ? -2 : Integer.parseInt(tempValue);
			
			String tempDate = eSysParam.selectSysValue("NO_OF_DAYS_FOR_EAPPROVAL_SUBMITTED_EXPIRY_DATE");
			int expriyDays = (tempDate == null) ? 13 : Integer.parseInt(tempDate) * -1;
			expriyDays = expriyDays - 1;

			String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			Log.debug("Send FA Firm Case Approval Reminder Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send FA Firm Case Approval Reminder End Range of Filter");
			Log.debug(endFilterStr);
			
			Log.info("Start to get pending for fa admin submission View");

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"pendingForFAFirm\"," + stFilterStr + ",\"0\",\"0\"]", "[\"01\",\"pendingForFAFirm\"," + endFilterStr + ",\"ZZZ\",\"ZZZ\"]");
			
			
			if (viewRows == null) {
				Log.debug("Null Send pending for fa admin approval case");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");
			
			if (rows.length() == 0){
				Log.debug("row.length == 0 Send pending for fa admin approval case");
				return;
			}
			
			JsonObject valueObj;
			String approvalId;

			Set<String> list = new HashSet<String>();
			Function func = new Function();
			for (int i = 0; i < rows.length(); i++){
				try {
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
//					approvalId = ApprovalUtil.getProxySupervisorCode(func.transformEleToString(valueObj.get("policyId")));
					approvalId = func.transformEleToString(valueObj.get("approvalCaseId"));
									
					list.add(approvalId);
				} catch (Exception exp) {
					Log.error(exp);
				}
			};
	
			List<String> noDupSet = new ArrayList<>();
			noDupSet.clear();
			noDupSet.addAll(list);
			
			handleFallinCase(noDupSet, stFilterStr, endFilterStr, expriyDays);
			
			Log.info("Send FA Firm Case Approval Reminder Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list, String stFilterStr, String endFilterStr, int expriyDays){
		int bNo=0;
		try {
			if (list.size() > 0) {
				Log.debug("No of FA Firm Case Approval Reminder Fall in Case: " + list.size());
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SEND_FAFIRM_APPROVALREMINDER", "P", new Date(), list.size());
				
				if (resultPreSub) {
					HashMap<String, String> fallInCase = new HashMap<String, String>();
					Function func = new Function();
					HashMap<String, String> faFirmProfile = new HashMap<String, String>();
					
					//Update Agents View Index
					Constant.CBUTIL.getRecordsByViewAfterIndex("agentWithDescendingOrder",  "[\"01\",\"agentCodeFirst\",\"0\",0]",  "[\"01\",\"agentCodeFirst\",\"ZZZ\",99999505729340000]");
					for (int i = 0; i < list.size(); i++) {
						String approvalId = (String) list.get(i);
						JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(approvalId));
						
						fallInCase  = new HashMap<String, String>();
//						fallInCase.put("proposalNumber", func.transformEleToString(approvalDoc.get("policyId")));
						if (func.transformEleToBoolean(approvalDoc.get("isShield"))) {
							fallInCase.put("proposalNumber", ApprovalUtil.getCSVString(approvalDoc.get("subApprovalList").getAsJsonArray()));
						} else {
							fallInCase.put("proposalNumber", func.transformEleToString(approvalDoc.get("approvalCaseId")));
						}
						
						fallInCase.put("productName", func.transformEleToString(approvalDoc.get("productName")));
						fallInCase.put("agentName", func.transformEleToString(approvalDoc.get("agentName")));
						fallInCase.put("adviserName", func.transformEleToString(approvalDoc.get("agentName")));
						fallInCase.put("submissionDate", NotificationsUtil.convertISOStringtoDisplay(func.transformEleToString(approvalDoc.get("submittedDate")), "dd/MM/YYYY"));
						fallInCase.put("proposerName", func.transformEleToString(approvalDoc.get("proposerName")));
						fallInCase.put("expiryDate", Function.getExpiryDateFrSubmitted( approvalDoc.get("submittedDate").getAsString(), expriyDays, "dd/MM/YYYY"));
						String agentId = func.transformEleToString(approvalDoc.get("agentId"));
						faFirmProfile  = func.getFAAdminProfileByAgentCode(agentId);
						fallInCase.put("email", faFirmProfile.get("email"));
						
						emailNotification(fallInCase);
					}

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static boolean emailNotification(HashMap<String, String> fallInCase){
		try {
			HashMap<String, String> validProps = new HashMap<String, String>();
			validProps.put("recipients", fallInCase.get("email"));
			String webDomain = EnvVariable.get("WEB_DOMAIN");
			validProps.put("actionLink", webDomain + "/goApproval");
			
			validProps.put("proposalNumber", fallInCase.get("proposalNumber"));
			validProps.put("productName", fallInCase.get("productName"));
			validProps.put("agentName", fallInCase.get("agentName"));
			validProps.put("adviserName", fallInCase.get("adviserName"));
			validProps.put("submissionDate", fallInCase.get("submissionDate"));
			validProps.put("proposerName", fallInCase.get("proposerName"));
			validProps.put("expiryDate", fallInCase.get("expiryDate"));
			
			Email email = NotificationsUtil.getEmailObject(NotificationsType.FA_FIRM_CASE_APPROVAL_REMINDER, validProps);
			
			NotificationsUtil.sendEmail(email);
			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
}
