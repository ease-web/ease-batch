package com.eab.batch.approvalnotification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendFAFirmReminder implements Job {
	private static final Set<String> ESCAPE_STATUS = new HashSet<String>(Arrays.asList(
		     new String[] {"A","R","E"}
	));
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public static void startBatch() {
		Log.info("Send FA Firm Reminder Notification Batch Job Start");
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_TO_SEND_NO_ACTION_EMAIL_FOR_FAFIRM");
			int controlValue = (tempValue == null) ? -7 : Integer.parseInt(tempValue);
			
			String tempDate = eSysParam.selectSysValue("NO_OF_DAYS_FOR_EAPPROVAL_SUBMITTED_EXPIRY_DATE");
			int expriyDays = (tempDate == null) ? 13 : Integer.parseInt(tempDate) * -1;
			expriyDays = expriyDays - 1;
			
			String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			Log.debug("Send FA Firm Reminder Notification Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send FA Firm Reminder Notification End Range of Filter");
			Log.debug(endFilterStr);
			
			Log.info("Start to get lastedit submission View");

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"lastedit\"," + stFilterStr + "," + stFilterStr + ",\"0\"]", "[\"01\",\"lastedit\"," + endFilterStr + "," + endFilterStr +",\"ZZZ\"]");
			
			if (viewRows == null) {
				Log.debug("Null Send FA Firm Reminder Notification");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");

			if (rows.length() == 0){
				Log.debug("row.length == 0 FA Firm Reminder Notification");
				return;
			}
			
			JsonObject valueObj;
//			String policyId;
			String approvalCaseId;
			String applicationId;
			String fullName ="";
			String submittedDate;
			String lastEditedDate;
			boolean isFACase;

			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();	
			HashMap<String, String> fallInCase = new HashMap<String, String>();
			HashMap<String, String> agentProfile = new HashMap<String, String>();
			Function func = new Function();
			
			for (int i = 0; i < rows.length(); i++){
				try {
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
					isFACase = func.transformEleToBoolean(valueObj.get("isFACase"));
//					policyId = func.transformEleToString(valueObj.get("policyId"));
					approvalCaseId = func.transformEleToString(valueObj.get("approvalCaseId"));
					applicationId = func.transformEleToString(valueObj.get("applicationId"));
					submittedDate = func.transformEleToString(valueObj.get("submittedDate"));
					lastEditedDate = func.transformEleToString(valueObj.get("lastEditedDate"));
					
					if (ESCAPE_STATUS.contains(func.transformEleToString(valueObj.get("status"))) || !isFACase) {
						continue;
					}
					
					JsonObject doc = func.transformObject(Constant.CBUTIL.getDoc(applicationId));
					if (doc == null) {
						continue;
					}
//					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(policyId));
					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(approvalCaseId));
					
					JsonElement elm = func.getAsPath(doc.getAsJsonObject(), "applicationForm/values/proposer/personalInfo/fullName");
					if (elm != null) {
						fullName = elm.getAsString();
					}
					
					fallInCase  = new HashMap<String, String>();
					fallInCase.put("submittedDate", func.transformEleToString(valueObj.get("submittedDate")));
					agentProfile = func.getAgentProfileInfo(func.transformEleToString(valueObj.get("agentId")));
					fallInCase.put("managerId", agentProfile.get("managerCode"));
					fallInCase.put("agentId", func.transformEleToString(valueObj.get("agentId")));
//					fallInCase.put("policyId", func.transformEleToString(valueObj.get("policyId")));
					fallInCase.put("approvalCaseId", func.transformEleToString(valueObj.get("approvalCaseId")));
					fallInCase.put("fullName", fullName);
					fallInCase.put("productName", approvalDoc.get("productName").getAsString());
					fallInCase.put("expiredDate", Function.getExpiryDateFrSubmitted( approvalDoc.get("submittedDate").getAsString(), expriyDays, "dd/MM/YYYY"));
					if (func.transformEleToBoolean(valueObj.get("isShield"))) {
						fallInCase.put("proposalNumber", ApprovalUtil.getCSVString(valueObj.get("subApprovalList").getAsJsonArray()));
					} else {
						fallInCase.put("proposalNumber", func.transformEleToString(valueObj.get("approvalCaseId")));
					}
					
					if (lastEditedDate.equalsIgnoreCase(submittedDate)) {
						list.add(fallInCase);
					}
				} catch (Exception exp) {
					Log.error("Handle Send Director Fall in case error: " + exp);
				}
			};
			
			handleFallinCase(list, expriyDays);
			
			Log.info("Send FA Firm Reminder Notification Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list, int expriyDays){
		int bNo=0;
		try {
			if (list.size() > 0) {
				Log.debug("No of FA Firm Reminder Notification Fall in Case: " + list.size());
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SENDFAFIRMNOTIFICATION", "P", new Date(), list.size());
				
				if (resultPreSub) {
					Function func = new Function();
					HashMap<String, String> agentProfile = new HashMap<String, String>();
					HashMap<String, String> managerProfile = new HashMap<String, String>();
					HashMap<String, String> directorProfile = new HashMap<String, String>();
					
					//Update Agents View Index
					Constant.CBUTIL.getRecordsByViewAfterIndex("agentWithDescendingOrder",  "[\"01\",\"agentCodeFirst\",\"0\",0]",  "[\"01\",\"agentCodeFirst\",\"ZZZ\",99999505729340000]");
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, String> noActionCase = (HashMap<String, String>) list.get(i);
						agentProfile = func.getAgentProfileInfo(noActionCase.get("agentId"));
						managerProfile = func.getAgentProfileInfo(noActionCase.get("managerId"));
						directorProfile = func.getAgentProfileInfo(managerProfile.get("managerCode"));
						
						noActionCase.put("agentEmail", agentProfile.get("email"));
						noActionCase.put("agentMobile", agentProfile.get("mobile"));
						noActionCase.put("agentName", agentProfile.get("name"));
						noActionCase.put("managerEmail", managerProfile.get("email"));
						noActionCase.put("managerMobile", managerProfile.get("mobile"));
						noActionCase.put("managerName", managerProfile.get("name"));
						noActionCase.put("directorEmail", directorProfile.get("email"));
						noActionCase.put("directorMobile", directorProfile.get("mobile"));
						noActionCase.put("directorName", directorProfile.get("name"));
						
						
						emailNotification(noActionCase, expriyDays);
					}

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static void emailNotification(HashMap<String, String> noActionCase, int expriyDays){
		try {
			
			String ccList = "";
			Function func = new Function();
			HashMap<String, String> faFirmProfile = new HashMap<String, String>();
			HashMap<String, String> validProps = new HashMap<String, String>();
			faFirmProfile  = func.getFAAdminProfileByAgentCode(noActionCase.get("agentId"));
			validProps.put("recipients", faFirmProfile.get("email"));
//			validProps.put("proposalNumber", noActionCase.get("policyId"));
			validProps.put("proposalNumber", noActionCase.get("proposalNumber"));
			validProps.put("productName", noActionCase.get("productName"));
			validProps.put("proposerName", noActionCase.get("fullName"));
			validProps.put("agentName", noActionCase.get("agentName"));
			validProps.put("adviserName", noActionCase.get("agentName"));
			validProps.put("managerName", noActionCase.get("managerName"));
			validProps.put("clientName", noActionCase.get("fullName"));
//			validProps.put("caseNumber", noActionCase.get("policyId"));
			validProps.put("caseNumber", noActionCase.get("proposalNumber"));
			
			//Convert the date format to email display format			
			validProps.put("expiryDate", Function.getExpiryDateFrSubmitted( noActionCase.get("submittedDate"), expriyDays, "dd/MM/YYYY"));
			validProps.put("submissionDate", NotificationsUtil.convertISOStringtoDisplay(noActionCase.get("submittedDate"), "dd/MM/YYYY"));
			
			validProps.put("cc", ccList);	
			Email email = NotificationsUtil.getEmailObject(NotificationsType.FA_FIRM_NOTIFICATION, validProps);
			
			NotificationsUtil.sendEmail(email);
		} catch (Exception e) {
			Log.error(e);
		}
	}
}
