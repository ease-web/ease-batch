package com.eab.batch.signatureexpiry;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.time.StopWatch;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.BATCH_SIGN_EXPIRY_TRX;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.eab.dao.INVALIDATED_SIGNED_CASE;
import com.eab.dao.SYSTEM_PARAM;
import com.google.gson.JsonObject;

public class SignatureExpiryJob implements Job {
	String JOB_NAME_KEY = "BATCH_JOB_SIGNATURE_EXPIRY";
	String EXPIRY_DAY_KEY = "NO_OF_DAYS_FOR_SIGN_EXPIRY";
	String DEF_EXPIRY_DAY = "-21";

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");

		stopWatch = null;
	}

	public void startBatch() {
		Log.info("App Signature Expiry start");
		
		boolean logCreated = false;
		int count = 0;
		int bNo = 0;
		int totRecords = 0;
		BATCH_LOG bLog = new BATCH_LOG();
		
		try {
			bNo = bLog.selectSeq();
			
//			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
//			String tempValue = eSysParam.selectSysValue(EXPIRY_DAY_KEY);
//			if (tempValue == null) {
//				tempValue = DEF_EXPIRY_DAY;
//			}
//			int expLimit = Integer.parseInt(tempValue);
			
			JSONObject sysParam = Constant.CBUTIL.getDoc("sysParameter");
			
			int expLimit = sysParam.has("signExpireDay") ? sysParam.getInt("signExpireDay") : 21;

			Calendar c = Calendar.getInstance();
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
			c.add(Calendar.DATE, -(expLimit-1));
			Date expSignDate = c.getTime(); 
			
			if (Constant.CBUTIL == null) {
		        String gatewayUser = EnvVariable.get("GATEWAY_USER");
		        String gatewayPW = EnvVariable.get("GATEWAY_PW");

				Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			}
			
			// retrieve target signature date 
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("signatureExpire", "[\"01\",0]", "[\"01\","+expSignDate.getTime()+"]");
			
			if (viewRows != null && viewRows.has("rows")) {
				JSONArray rows = viewRows.getJSONArray("rows");				
				if (rows != null) {
					totRecords = rows.length();
					
					Log.debug("Start insert to Batch log table");
					logCreated = bLog.create(bNo, JOB_NAME_KEY, "P", new Date(), totRecords);
					
					if (logCreated) {
						for (int i = 0; i < totRecords; i++) {
							JSONObject entry = rows.getJSONObject(i);
							JSONObject valueObj = entry.getJSONObject("value");
							String appId = valueObj.getString("appId");
							BATCH_SIGN_EXPIRY_TRX bseDao = new BATCH_SIGN_EXPIRY_TRX();
							boolean trxCreated = false;
							String remark = "New"; 
							try {
								String polNo = valueObj.has("polNo")?valueObj.getString("polNo"):"";
								String bundleId = valueObj.has("bundleId")?valueObj.getString("bundleId"):"";
								long signTime = valueObj.has("signDate")?valueObj.getLong("signDate"):0;
								
								String payMethod = valueObj.has("payMethod")?valueObj.getString("payMethod"):"";
								String payStatus = valueObj.has("payStatus")?valueObj.getString("payStatus"):"";
								
						        boolean isFullySigned = valueObj.has("isFullySigned")?valueObj.getBoolean("isFullySigned"):false;
						        boolean isInitialPaymentCompleted = valueObj.has("isInitialPaymentCompleted")?valueObj.getBoolean("isInitialPaymentCompleted"):false;
						        String docType = valueObj.has("docType")?valueObj.getString("docType"):"";
			
								Date signDate = null;
								if (signTime > 0) {
									signDate = new Date(signTime);
								} else {
									signDate = new Date(0);
								}

								// create sign expiry record
								trxCreated = bseDao.create(appId, bNo, polNo, bundleId, signDate, expLimit, payMethod+"("+payStatus+")", remark);
								
								// update application doc in coucbhase
								JSONObject bundle = Constant.CBUTIL.getDoc(bundleId);
								java.util.Date timestamp = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime();

								if (bundle.has("applications")) {
									JSONArray apps = bundle.getJSONArray("applications");
									boolean found = false;
									for (int j = 0; apps.length() > j;  j++ ) {
										JSONObject app = apps.getJSONObject(j);
										if (app.has("applicationDocId") && appId.equals(app.getString("applicationDocId"))) {
											if (isFullySigned) {
												app.put("appStatus", "INVALIDATED_SIGNED");
											} else {
												app.put("appStatus", "INVALIDATED");
											}
											app.put("invalidateReason", "Signature Expired");
											app.put("invalidateDate", timestamp.getTime());
											
											found = true;
											break;
										}
									}									
									if (found) {
										String bundleUpdResult = Constant.CBUTIL.UpdateDoc(bundleId, bundle.toString());
										remark = "Signature expired: Bundle " + bundleUpdResult + ".";
										Log.info("Signature expired: "+ appId + " on bundle = "+ bundleId +  " : result " + bundleUpdResult);
									} else {
										Log.error("Sigature expired, invalid bundle, no application ?");
									}
								} else {
									Log.error("Sigature expired, invalid bundle, no application ?");
								}
								
								
								JSONObject updateDoc = Constant.CBUTIL.getDoc(appId);

								// for refund, need to submit (signed) paid application 
								if (isFullySigned && isInitialPaymentCompleted) {
									BATCH_SUBMISSION_REQUEST bSubReq = new BATCH_SUBMISSION_REQUEST();
									int bNoforSubReq = bSubReq.selectSeq();
									boolean resultSubReq = false;
									String statusToTriggerSubmission = "Invalidated";
									if(!bSubReq.hasPrimaryKey(appId, statusToTriggerSubmission) && docType.equalsIgnoreCase("application")) {
										resultSubReq = bSubReq.create(appId, statusToTriggerSubmission, bNoforSubReq, "R", "R", "R", "EASE_BATCH");	
										if (resultSubReq) {
											updateDoc.put("isSubmittedStatus", true);
											updateDoc.put("applicationSubmittedDate", timestamp.getTime());
											remark += " Application submitted.";
										}
									} else if (docType.equalsIgnoreCase("masterApplication")) {
										updateDoc.put("isSubmittedStatus", true);
										updateDoc.put("applicationSubmittedDate", timestamp.getTime());
										remark += " Application submitted.";
									}
								}
								updateDoc.put("isInvalidated", true);
								
								String appUpdResult = Constant.CBUTIL.UpdateDoc(appId, updateDoc.toString());
								remark += " Application " + appUpdResult + ".";

								bseDao.update(appId, bNo, remark);

								count++;
							} catch(Exception ex) {
								ex.printStackTrace();
								Log.error("data missing: skip application:" + appId + " - " + valueObj.toString());
								
								if (trxCreated) {
									bseDao.update(appId, bNo, remark + " > Exception:" + ex.getMessage());
								} else {
									bseDao.create(appId, bNo, "", "", new Date(0), expLimit, "", remark + " > Exception:" + ex.getMessage());
								}
							}							
						}
						
						bLog.update(bNo, count, "C", "BATCH");
					}
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
			try {
				if (!logCreated) {
					logCreated = bLog.create(bNo, JOB_NAME_KEY, "F", new Date(), totRecords);
				} else {
					bLog.update(bNo, count, "F", "BATCH");
				}
			} catch (Exception e) {
				Log.error("failure to update signature expiry batch log");
			}
		} finally {
			try {
				if (!logCreated) {
					bLog.create(bNo, JOB_NAME_KEY, "F", new Date(), -1);
				}
			} catch (Exception ex2) {
				Log.error("failure to create signature expiry batch log in final");
			}
			Log.info("App Signature Expiry end");
		}
	}

}
