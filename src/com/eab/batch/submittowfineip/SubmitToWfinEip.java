package com.eab.batch.submittowfineip;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.SYSTEM_PARAM;
import com.google.gson.JsonObject;

public class SubmitToWfinEip implements Job {
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private static final String OP_MODE = "OP_MODE";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		String tempValue = "M";
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			tempValue = eSysParam.selectSysValue(OP_MODE);
		} catch (Exception e) {
			Log.error(e);
		}
		if (tempValue.equalsIgnoreCase("O")){
			startBatch();
		} else {
			Log.info("DB OP_MODE in SYSTEM_PARAM is Maintenance Mode");
		}
		
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	
	
	public void startBatch() {
		try {
			JsonObject body = new JsonObject();
			body.addProperty("callByBatch", true);
			
			Log.info("Start to call api to send WFI and RLS");
	
			Log.info("Call WFI and RLS Submission Api: " + Function.callpostApi(body, DOMAIN + "/WfiRlsSubmission/submit"));
		
		} catch (Exception e) {
			Log.error(e);
		}
	}
}
