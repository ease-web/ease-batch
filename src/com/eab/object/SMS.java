package com.eab.object;

public class SMS {
	private String mobileNo;
	private String smsMsg;
	
	public SMS() {
	}
	
	public SMS(String mobileNo, String smsMsg) {
		this.mobileNo = mobileNo;
		this.smsMsg = smsMsg;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSmsMsg() {
		return smsMsg;
	}

	public void setSmsMsg(String smsMsg) {
		this.smsMsg = smsMsg;
	}

	@Override
	public String toString() {
		return "SMS [mobileNo=" + mobileNo + ", smsMsg=" + smsMsg + "]";
	}
}
